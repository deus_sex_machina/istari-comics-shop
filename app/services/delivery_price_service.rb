class DeliveryPriceService
  def initialize(city, weight)
    @city = city
    @weight = weight
  end

  def process
    @packaging_cost = PackingCost.last.cost
    @weight = @weight.to_i

    postcalc_response = HTTP.get(ENV["POSTCALC_URL"], params: { f: ENV["FROM_INDEX"], t: @city, w: @weight, o: "JSON" })
    postcalc_response = ActiveSupport::Gzip.decompress(postcalc_response)
    @json_data = ActiveSupport::JSON.decode(postcalc_response)
    @price = choose_delivery_type
  end

  private

  def choose_delivery_type
    train = @json_data["Отправления"]["ЦеннаяПосылка"]["Доставка"].to_i if data_train?
    if train.blank? || train == 0
      avia = @json_data["Отправления"]["ЦеннаяАвиаПосылка"]["Доставка"].to_i if data_avia?
      if avia.blank? || avia == 0
        price = nil
      else
        price = avia + @packaging_cost
      end
    else
      price = train + @packaging_cost
    end
  end

  def data_train?
    @json_data["Отправления"] &&
    @json_data["Отправления"]["ЦеннаяПосылка"] &&
    @json_data["Отправления"]["ЦеннаяПосылка"]["Доставка"]
  end

  def data_avia?
    @json_data["Отправления"] &&
    @json_data["Отправления"]["ЦеннаяАвиаПосылка"] &&
    @json_data["Отправления"]["ЦеннаяАвиаПосылка"]["Доставка"]
  end
end
