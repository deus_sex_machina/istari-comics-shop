class CheckItemsService
  def initialize(cart_items)
    @cart_items = cart_items
  end

  def unavailability
    @unavailable_items = []
    @cart_items.each { |item| check_each item }
    @unavailable_items
  end

  def available_count?
    count = true
    @cart_items.each do |item|
      count = false if item[:count] > 10
    end
    count
  end

  private

  def check_each(item)
    @in_stock = Product.find(item[:product_id]).in_stock
    @in_stock = 0 if @in_stock.blank?
    if item[:count] > @in_stock
      @unavailable_items << { "product_id"=> item[:product_id], "count"=>item[:count] - @in_stock }
    end
  end
end
