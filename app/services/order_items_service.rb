class OrderItemsService
  def initialize(cart_items, order)
    @cart_items = cart_items
    @order = order
  end

  def create
    @cart_items.each do |item|
      product = Product.find(item[:product_id])
      @order_item = @order.order_items.new(product_id: item[:product_id], count: item[:count])

      if product.preorder && product.preorder.current?
        @order_item.preorder_id = product.preorder.id
      end
      @order_item.save!
    end
  end
end
