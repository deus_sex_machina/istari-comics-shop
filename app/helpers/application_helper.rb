module ApplicationHelper
  def alert_type(type)
    "alert-" + case type
               when "alert"
                 "danger"
               else
                 "warning"
               end
  end
end
