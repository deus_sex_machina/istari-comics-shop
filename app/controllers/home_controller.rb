class HomeController < ApplicationController
  expose(:features) { Feature.includes(:page) }
  expose :slides
  expose :partners
  expose(:products_by_categories) { Category.includes(:products) }
  expose_decorated(:preorders) { Preorder.actual.includes(:products) }
end
