class SearchController < ApplicationController
  def index
    @query = params[:q]
    @result_by_categories = Category.search_products(@query)

    render layout: request.get?
  end
end
