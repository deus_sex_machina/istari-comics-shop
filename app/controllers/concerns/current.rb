module Current
  extend ActiveSupport::Concern

  included do
    before_action :check_user_for_current
  end

  private

  def check_user_for_current
    if params[:user_id] != "current"
      render nothing: true, status: :unauthorized
    end
  end
end
