class RegistrationsController < DeviseTokenAuth::RegistrationsController
  # before_filter :configure_sign_up_params, only: [:create]

  protected

  # def configure_sign_up_params
  #   logger = Logger.new(STDOUT)
  #   logger.debug "Hello"
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:full_name])
  # end

  def sign_up_params
    params.permit(:email, :password, :password_confirmation, :first_name,
                  :last_name, :middle_name)
  end
end
