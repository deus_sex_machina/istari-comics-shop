class FrontController < ActionController::Base
  protect_from_forgery with: :null_session

  layout "application"

  def index
    render 'front/index'
  end
end
