class SocialNetworksController < ApplicationController
  def index
    render json: SocialNetwork.all
  end
end
