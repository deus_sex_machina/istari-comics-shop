class Api::Callbacks::Payment::PaymasterController < Api::BaseController
  before_action :fetch_paid
  before_action :fetch_order

  %i( ensure_merchaint ensure_order_paid ensure_has_delivery ).each do |callback|
    before_action callback, only: :notification
  end

  %i( ensure_status ensure_price ).each do |callback|
    before_action callback, only: :confirmation
  end

  def notification
    params.default = false

    @order.update!(
      defer_shipping: params[:defer_shipping],
      await_preorder: !params[:before_finish],
      paid: @paid
    )

    render nothing: true
  end

  def confirmation
    render plain: "yes"
  end

  private

  def fetch_paid
    @paid = params[:LMI_PAYMENT_AMOUNT].to_i
  end

  def fetch_order
    order_id = params[:LMI_PAYMENT_NO]

    unless Order.exists?(order_id)
      render nothing: true
      return
    end

    @order = Order.find(order_id)
  end


  # Notification callbacks

  def ensure_merchaint
    render nothing: true unless ENV['MERCHANT_ID'] == params[:LMI_MERCHANT_ID]
  end

  def ensure_order_paid
    render nothing: true if @order.paid?
  end

  def ensure_has_delivery
    render nothing: true unless @order.delivery?
  end


  # Confirmation callbacks

  def ensure_status
    render plain: "no" unless @order.processed?
  end

  def ensure_price
    render plain: "no" unless @order.price - @order.paid == @paid
  end
end
