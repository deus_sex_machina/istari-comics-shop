class Api::Callbacks::Delivery::CheckoutController < Api::BaseController
  before_action :load_params, only: :success

  def session
    checkout_response = HTTP.get("http://platform.checkout.ru/service/login/ticket/#{ENV["CHECKOUT_KEY"]}")
    parsed_json = ActiveSupport::JSON.decode(checkout_response)
    render json: {
      token: parsed_json['ticket'],
      "packing-cost": PackingCost.last.cost
    }
  end

  def success
    order_id = params[:order_id]
    order = Order.find(order_id)
    order.update!(
      delivery: true,
      comment: @comment,
      external_id: @external_id,
      cost: @cost,
      price: @order_cost,
      address: @address,
      full_name: @full_name,
      delivery_type: :checkout,
      country: "Russia",
      index: @delivery_post_index
    )

    redirect_to "/my/orders/#{order_id}/payment"
  end

  private

  def load_params
    @comment = params[:comment]
    @external_id = params[:orderId]
    @cost = params[:deliveryCost]
    @order_cost = params[:deliveryOrderCost]
    @address = params[:address]
    @full_name = params[:clientFIO]
    @delivery_post_index = params[:deliveryPostindex]
  end
end
