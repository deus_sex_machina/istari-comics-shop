class Api::SeriesController < Api::BaseController
  def index
    @series = Series.select(%i{ id name description })
    render json: @series
  end
end
