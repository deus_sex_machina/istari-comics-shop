class Api::PagesController < Api::BaseController
  def index
    render json: Page.all
  end
end
