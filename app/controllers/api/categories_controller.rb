class Api::CategoriesController < Api::BaseController
  def index
    categories = Category.select(%i{ id name description })
    render json: categories
  end
end
