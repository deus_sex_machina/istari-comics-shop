class Api::SocialNetworksController < Api::BaseController  
  def index
    @social_networks = SocialNetwork.all
    render json: @social_networks
  end
end
