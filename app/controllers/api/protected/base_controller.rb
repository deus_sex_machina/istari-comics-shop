class Api::Protected::BaseController < Api::BaseController
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :authenticate_api_user!
end
