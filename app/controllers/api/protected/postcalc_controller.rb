class Api::Protected::PostcalcController < Api::Protected::BaseController
  before_action :get_postcalc

  def index
    price = DeliveryPriceService.new(@postcalc[:index], @postcalc[:weight]).process

    if price.present?
      render json: {price: price}
    else
      render nothing: true, status: 400
    end
  end

  private

  def get_postcalc
    @postcalc = params.require(:postcalc).permit(:index, :weight)
  end
end
