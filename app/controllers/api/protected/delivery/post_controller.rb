class Api::Protected::Delivery::PostController < Api::BaseController
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :move_access_data_from_params_to_headers
  before_action :authenticate_api_user!
  before_action :load_params

  def create
    order = current_api_user.orders.find(params[:order_id])
    cost = DeliveryPriceService.new(@index, order.weight).process

    price = order.price + cost
    order.update!(
      delivery: true,
      comment: @comment,
      country: @country,
      address: @address,
      full_name: "#{@last_name} #{@first_name} #{@middle_name}",
      cost: cost,
      price: price,
      index: @index
    )

    redirect_to "/my/orders/#{order.id}/payment"
  end

  private

  def move_access_data_from_params_to_headers
    %w( access-token client expiry uid ).each do |key|
      request.headers[key] = params[key]
    end
  end

  def load_params
    @comment = params[:comment]
    @country = params[:country]
    @address = params[:address]
    @first_name = params[:first_name]
    @last_name = params[:last_name]
    @middle_name = params[:middle_name]
    @index = params[:index]
  end
end
