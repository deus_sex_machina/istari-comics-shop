class Api::Protected::ReviewsController < Api::Protected::BaseController
  def create
    current_api_user.reviews.create(reviews_params)
  end

  private

  def reviews_params
    params.permit(:text)
  end
end
