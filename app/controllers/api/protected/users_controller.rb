class Api::Protected::UsersController < Api::Protected::BaseController
  def current
    @user = current_api_user
  end

  def update
    @user = current_api_user
    @user.update!(profile_params)
    render json: @user
  end

  private

  def profile_params
    params.permit(
      :first_name,
      :middle_name,
      :last_name,
      :phone,
      :address,
      :index,
      :locality,
      :email
    )
  end
end
