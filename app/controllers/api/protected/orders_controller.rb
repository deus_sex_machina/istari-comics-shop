class Api::Protected::OrdersController < Api::Protected::BaseController
  before_action :create_order, only: :create
  before_action :load_orders_ids, only: :merge
  before_action :load_orders, only: :merge
  before_action :merge_order_items, only: :merge
  before_action :ensure_if_avaialable, only: %i( merge create )
  before_action :save_order, only: %i( merge create )

  def index
    @orders = current_api_user.orders.includes(:order_items)
  end

  def create
    render json: @order.id
  end

  def merge
    @old_order.delete
    render json: @order.id
  end

  private

  def cart_items
    params
      .require(:cart_items)
      .map { |item| item.permit(:product_id, :count) }
  end

  def create_order
    @order = current_api_user.orders.new
    @order.order_items << cart_items.map { |params| OrderItem.new(params) }
  end

  def load_orders_ids
    @order_id, @old_order_id =
      params.values_at(:order_id, :old_order_id)

    render nothing: true, status: :bad_request if @order_id == @old_order_id
  end

  def load_orders
    @order =
      current_api_user.orders
        .includes(:order_items)
        .created
        .find(@order_id)

    @old_order =
      current_api_user.orders
        .includes(:order_items)
        .where(status: %i( created processed paid stocked ))
        .find(@old_order_id)
  end

  def merge_order_items
    @old_order.order_items.each do |old_order_item|
      product_id = old_order_item.product_id

      order_item = @order.order_items.detect { |oi| oi.product_id == product_id }

      if order_item.nil?
        order_item = @order.order_items.new(product_id: product_id, count: 0)
      end

      order_item.count += old_order_item.count
      order_item.preorder_id = old_order_item.preorder_id
    end
  end

  def ensure_if_avaialable
    unless @order.available_or_in_preorder_now?
      render nothing: true, status: :bad_request
    end
  end

  def save_order
    @order.price  = @order.pure_price
    @order.weight = @order.pure_weight + 200

    @order.order_items.each(&:save!)
    @order.save!
  end
end
