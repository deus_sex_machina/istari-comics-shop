class Api::ProductsController < Api::BaseController
  def index
    @products = Product.includes(:reviews)
  end
end
