class Api::IndicesController < Api::BaseController
  before_action :get_query

  def index
    result = PostcalcLightCity
      .where("pindex like ?", "#{@query}%")
      .select("pindex as index", :city)

    render json: result
  end

  private

  def get_query
    @query = params.fetch(:q, "")
  end
end
