class ReviewsController < ApplicationController
  def index
    render json: Product
      .includes(reviews: :user)
      .find(params[:product_id])
      .reviews
  end

  # before_action :authenticate_user!
  # before_action :authorize_user!, except: %i( index show create )
  # before_action :apply_user_and_product

  # expose :review, attributes: :review_params
  # expose(:product) { Product.find(params.require(:product_id)) }

  # def create
  #   messages = ["Ваш отзыв принят к рассмотрению"]
  #   status = :ok
  #
  #   unless review.save && verify_recaptcha(model: review)
  #     status = :bad_request
  #     messages = review.errors.full_messages
  #   end
  #
  #   render json: { messages: messages }, status: status
  # end
  #
  # private
  #
  # def apply_user_and_product
  #   review.user = current_user
  #   review.product = product
  # end
  #
  # def review_params
  #   params.require(:review).permit(:text, { images: [] })
  # end
  #
  # def authorize_user!
  #   authorize(review, :manage?)
  # end
end
