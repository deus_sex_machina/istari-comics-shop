#= require jquery.countdown
#= require jquery.countdown-ru

counterLayout = """
  <span class="text-lowercase font-weight-bold">
    <span class="h5 font-weight-bold">{dn}</span> {dl},
    <span class="h5 font-weight-bold">{hn}</span> {hl},
    <span class="h5 font-weight-bold">{mn}</span> {ml},
    <span class="h5 font-weight-bold">{sn}</span> {sl}
  </span>
"""

$ ->
  $ "[data-till]"
    .each ->
      $preorder = $ @

      untilDate = $preorder.attr "data-till"
      untilDate = new Date untilDate

      $preorder.countdown
        until: untilDate
        description: $preorder.attr "data-text"
        format: "DHMS"
        layout: counterLayout
        onExpiry: ->
          do location.reload
