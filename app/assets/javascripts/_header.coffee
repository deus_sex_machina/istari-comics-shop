NO_BORDER_CLASS = "navbar-istari--no-border"

$ ->
  $window = $ window
  $header = $ ".navbar-istari"

  $window.scroll ->
    pos = do $window.scrollTop

    if pos == 0
      $header.addClass NO_BORDER_CLASS
    else
      $header.removeClass NO_BORDER_CLASS
