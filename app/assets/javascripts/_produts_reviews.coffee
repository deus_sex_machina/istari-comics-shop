$ ->
  $tools = $ ".tools"

  newAlert = (message, status) ->
    $alert = $ """
      <div class="alert alert-#{status}">
        <div class="container">
          #{message}
          <button class="close" type="button">&times;</button>
        </div>
      </div>
    """

  pushAlert = (message, status) ->
    $alert = newAlert message, status
    $tools.append $alert
    do $alert.shopAlert

  pushAlerts = (messages, status) ->
    messages.forEach (m) ->
      pushAlert m, status

  editor = CKEDITOR.instances["review_text"]
  $form = $ "#new_review"
  $icon = $form.find "[type=submit] > .fa"

  $form
    .on "ajax:send", ->
      editor.setReadOnly true
      $icon.removeClass "hidden-xs-up"
      console.log "send"
    .on "ajax:success", (e, data, status, xhr) ->
      pushAlerts xhr.responseJSON.messages, "success"
      do editor.updateElement
      editor.setData ""
    .on "ajax:error", (e, xhr, status, error) ->
      pushAlerts xhr.responseJSON.messages, "danger"
    .on "ajax:complete", ->
      editor.setReadOnly false
      $icon.addClass "hidden-xs-up"
