timeout =
  (dur, cb) ->
    setTimeout cb, dur

$.fn.shopAlert = ->
  $this = $ @

  do $this.hide
  do $this.slideDown

  hide = ->
    do $this.slideUp

  timeout 5000, ->
    do hide

  $this.on "click", ->
    do hide

$ ->
  $alerts = ($ ".alert")
  do $alerts.shopAlert
