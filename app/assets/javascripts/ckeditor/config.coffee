CKEDITOR.editorConfig = (config) ->
  config.toolbar = "mini"
  config.skin = "bootstrapck"
  config.extraPlugins = ["autogrow", "autolink", "videoembed"].join ","
  config.toolbar_mini = [
    ["Bold", "Italic", "Underline", "Strike"]
    ["Blockquote", "Link", "Image"]
    ["BulletedList", "NumberedList", "Indent", "Outdent"]
    ["Styles", "Format"]
    ["VideoEmbed"]
  ]
