#= require _produts_reviews

$ ->
  $window = $ window
  $description = $ ".product__description"
  $counter = $ ".product__counter"
  $less = $counter.find "#counter-less"
  $more = $counter.find "#counter-more"
  $input = $counter.find "input"
  $price = $ ".product #price"
  $cost = $ ".product #cost"

  $description.readmore
    collapsedHeight: 125
    moreLink: "<a href='#'>Читать далее</a>"
    lessLink: "<a href='#'>Свернуть</a>"

  price = do $price.html
  price = Number price

  value = ->
    val = do $input.val
    Number val

  setValue = (val) ->
    val ||= 0
    val = Math.max val, 1
    val = Math.min val, 10
    $input.val val
    $cost.html price * val

  $less.click ->
    val = do value
    setValue val - 1

  $more.click ->
    val = do value
    setValue val + 1

  $input.on "keyup keypress blur change", ->
    val = do value
    setValue val
