ANIMATION_DURATION = 100

$ ->
  $window = $ window
  $menu = $ "#collapse-header"
  $menuItems = $menu.find ".nav-item"
  $menuItem = $ "#search-button"
  $bar = $ ".search-bar"
  $form = $bar.find "form"
  $input = $form.find "input"
  $root = $ "#root"
  $button = $bar.find "button"
  $icon = $button.find "i"

  $window.on "popstate", ->
    do location.reload

  if $bar.hasClass "hidden-xs-up"
    do $bar.hide
    $bar.removeClass "hidden-xs-up"

  $menuItem.on "click", (event) ->
    do event.preventDefault
    $menu.collapse "hide"
    visible = $bar.is ":visible"
    $bar.slideToggle ANIMATION_DURATION
    if not visible
      do $input.focus

  $input.on "focus", ->
    $this = $ @
    value = do $this.val
    $this.val value

  firstSearch = true
  saveState = (url, query) ->
    saver = null

    if firstSearch
      saver = history.pushState
      firstSearch = false
      console.log 1
    else
      saver = history.replaceState

    saver.call history, history.state, null, "#{url}?#{query}"

  search = ->
    $menuItems.removeClass "active"
    $menuItem.addClass "active"

    $icon.addClass "fa-refresh rotate"
    url = $form.attr "action"
    query = do $form.serialize

    $.post url, query, (html) ->
      saveState url, query
      $root.html html
      $icon.removeClass "fa-refresh rotate"
      do window.initProductPreviews
      $ "html, body"
        .animate scrollTop: 0

  debouncedSearch = _.debounce search, 200

  $input.on "keyup", (e) ->
    if e.key == "Escape"
      $bar.slideUp ANIMATION_DURATION
    else
      do debouncedSearch

  $button.on "click", (event) ->
    do event.preventDefault
    do search
