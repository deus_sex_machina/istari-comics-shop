#= require jquery
#= require jquery_ujs
#= require tether
#= require bootstrap
#= require lodash
#= require ckeditor/init
#= require readmore

#= require _alerts
#= require _search_bar
#= require _products
#= require _header
#= require _countdown
