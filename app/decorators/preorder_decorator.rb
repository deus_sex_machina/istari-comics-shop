class PreorderDecorator < ApplicationDecorator
  delegate :name, :description, :products, :current?, :upcoming?,
    :start, :finish

  def background
    case
    when object.current?
      "bg-danger"
    when object.upcoming?
      "bg-warning"
    end
  end
end
