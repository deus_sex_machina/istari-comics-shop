class FeatureDecorator < ApplicationDecorator
  delegate :title, :description, :icon, :page
end
