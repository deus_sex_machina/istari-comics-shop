class AuthorDecorator < ApplicationDecorator
  delegate :full_name, :description, :products
end
