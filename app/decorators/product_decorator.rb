class ProductDecorator < ApplicationDecorator
  delegate :title, :category, :series, :cover, :price,
    :description, :preorder

  delegate :name, to: :category, prefix: true
  delegate :name, to: :series, prefix: true
  delegate :path, to: :cover, prefix: true

  def author_product_maps
    object.author_product_maps.includes(:author)
  end

  def reviews
    object.reviews
      .where(status: :confirmed)
      .includes(:user)
      .order(created_at: :desc)
  end

  def characteristics
    all_characteristics
      .reject do |c|
        list = c[:list].reject { |el| el[:value].empty? }
        list.empty?
      end
  end

  private

  def groups_characteristics
    %i( category series ).map do |field|
      name = Product.human_attribute_name(field)
      link = object.send(field)
      value = link.name

      { name: name, list: [{ value: value, link: link }] }
    end
  end

  def authors_characteristics
    author_product_maps.map do |author_product_map|
      name = AuthorProductMap.roles[author_product_map.role]
      link = author_product_map.author
      value = link.full_name

      { name: name, list: [{ value: value, link: link }] }
    end
  end

  def countries_characteristics
    countries = object.countries
    list = countries.map { |c| { value: c.name, link: c } }

    { name: "Страны", list: list }
  end

  def static_characteristics
    %i( rating article isbn pages size weight format ).map do |field|
      name = Product.human_attribute_name(field)
      value = object.send(field).to_s

      { name: name, list: [{ value: value, link: nil }] }
    end
  end

  def all_characteristics
    all = []
    all << groups_characteristics
    all << authors_characteristics
    all << countries_characteristics
    all << static_characteristics

    all.flatten(1)
  end
end
