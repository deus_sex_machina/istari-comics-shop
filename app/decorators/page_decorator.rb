class PageDecorator < ApplicationDecorator
  delegate :title, :content
end
