class SeriesDecorator < ApplicationDecorator
  delegate :name, :description, :products
end
