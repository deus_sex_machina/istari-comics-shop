class CountryDecorator < ApplicationDecorator
  delegate :name, :description, :products
end
