class CategoryDecorator < ApplicationDecorator
  delegate :name, :description, :products
end
