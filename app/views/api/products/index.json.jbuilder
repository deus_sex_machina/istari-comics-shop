json.array! @products do |product|
  json.id product.id
  json.title product.title
  json.pages product.pages
  json.weight product.weight
  json.description product.description
  json.article product.article
  json.rating product.rating
  json.format product.format
  json.size product.size
  json.isbn product.isbn
  json.price product.price
  json.available product.public_in_stock
  json.reviews product.reviews.confirmed.order(created_at: :desc)
  json.set! "category-id", product.category_id
  json.set! "series-id", product.series_id
  json.set! "preorder-id", product.preorder_id
  json.set! "cover-url", product.cover.url
  json.set! "cover-thumb", product.cover.thumb.url
  json.set! "preview-pdf", product.preview_pdf_url
  json.set! "preview-cbr", product.preview_cbr_url
  json.status I18n.t("product.status.#{product.status}")
  json.original_volumes product.original_volumes
  json.russian_volumes product.russian_volumes
end
