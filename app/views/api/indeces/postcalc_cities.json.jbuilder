json.message "ok"
json.cities @postcalc_cities do |city|
  json.city city.city
  json.index city.pindex
end
