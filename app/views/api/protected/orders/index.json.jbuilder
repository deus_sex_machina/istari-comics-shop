json.array! @orders do |order|
  json.id order.id
  json.status I18n.t("order.status.#{order.status}")
  json.weight order.weight
  json.price order.price
  json.created_at order.created_at

  json.order_items order.order_items do |item|
    json.id item.id
    json.product_id item.product_id
    json.preorder_id item.preorder_id
    json.count item.count
    json.available item.available?
    json.price item.price
    json.weight item.weight
  end
end
