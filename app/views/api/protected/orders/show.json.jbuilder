json.id @order.id
json.status @order.status
json.set!("order-items", @order.order_items) do |item|
  json.id item.id
  json.set!("product-id", item.product_id)
  json.set!("preorder-id", item.preorder_id)
  json.count item.count
  json.available item.available
end
