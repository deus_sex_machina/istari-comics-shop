json.id @user.id
json.email @user.email
json.set!("first-name", @user.first_name)
json.set!("middle-name", @user.middle_name)
json.set!("last-name", @user.last_name)
