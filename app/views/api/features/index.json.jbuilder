json.array! @features do |feature|
  json.id feature.id
  json.title feature.title
  json.description feature.description
  json.icon feature.icon
  json.set!("page-id", feature.page_id)
end
