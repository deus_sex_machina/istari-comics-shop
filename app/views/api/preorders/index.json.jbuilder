json.array! @preorders do |preorder|
  json.id preorder.id
  json.name preorder.name
  json.description preorder.description
  json.start preorder.start
  json.finish preorder.finish
  json.products preorder.products.ids
end
