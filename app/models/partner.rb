class Partner < ActiveRecord::Base
  validates :logo, presence: true

  mount_uploader :logo, PartnerUploader
end
