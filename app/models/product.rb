class Product < ActiveRecord::Base
  has_many :author_product_maps
  has_many :authors, through: :author_product_maps
  has_many :genre_product_maps
  has_many :genres, through: :genre_product_maps
  has_many :product_images
  has_many :reviews

  belongs_to :series
  belongs_to :category
  belongs_to :preorder

  has_and_belongs_to_many :countries

  validates :title, presence: true
  validates :price, presence: true
  validates :cover, presence: true
  validates :series, presence: true
  validates :category, presence: true

  after_save :update_orders

  mount_uploader :cover, ProductUploader
  mount_uploader :preview_pdf, PreviewPdfUploader
  mount_uploader :preview_cbr, PreviewCbrUploader

  enum status: {
    completed: "completed",
    ongoing: "ongoing",
  }

  # Returns true if there are at least
  # `count` (one by default) products in stock
  def available?(count = 1)
    in_stock >= count
  end

  # Returns true if product is in preorder now
  def in_preorder_now?
    preorder_id? && preorder.current?
  end

  # Reserves `count` products from stock
  def reserve!(count)
    update!(in_stock: in_stock - count)
  end

  # Frees up `count` products to stock
  def free!(count)
    update!(in_stock: in_stock + count)
  end

  def public_in_stock
    [in_stock, 10].min
  end

  private

  # Try to change status to :stocked
  def update_orders
    Order.paid.each(&:save!) if in_stock_changed?
  end
end
