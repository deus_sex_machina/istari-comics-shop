class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User

  devise :database_authenticatable, :registerable, :confirmable,
    :recoverable, :rememberable, :trackable, :validatable

  # before_validation :set_provider
  # def set_provider
  #   self[:provider] = "email" if self[:provider].blank?
  # end
  #
  # before_validation :set_uid
  # def set_uid
  #   self[:uid] = self[:email] if self[:uid].blank? && self[:email].present?
  # end

  # before_validation :set_confirm_success_url
  # def set_confirm_success_url
  #   set[:confirm_success_url] = request.host
  # end

  validates :first_name, presence: true
  validates :middle_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true

  has_many :orders
  has_many :reviews

  def display_name
    "#{last_name} #{first_name} #{middle_name}"
  end

  def full_name
    "#{last_name} #{first_name} #{middle_name}"
  end
end
