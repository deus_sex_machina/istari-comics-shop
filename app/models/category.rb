class Category < ActiveRecord::Base
  has_many :products

  validates :name, presence: true

  def self.search_products(query)
    query = query.to_s.mb_chars.downcase.strip

    return [] if query.empty?

    eager_load(:products).where("LOWER(products.title) like ?", "%#{query}%")
  end
end
