class SocialNetwork < ActiveRecord::Base
  validates :icon, presence: true
  validates :link, presence: true
end
