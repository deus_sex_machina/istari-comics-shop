class Genre < ActiveRecord::Base
  has_many :genre_product_maps
  has_many :products, through: :genre_product_maps
end
