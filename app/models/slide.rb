class Slide < ActiveRecord::Base
  validates :img, presence: true

  mount_uploader :img, SlideUploader
end
