class Feature < ActiveRecord::Base
  belongs_to :page

  validates :title, presence: true
  validates :description, presence: true
  validates :icon, presence: true
end
