class AuthorProductMap < ActiveRecord::Base
  belongs_to :product
  belongs_to :author

  enum role: {
    author: "Автор",
    story: "Сюжет",
    art: "Рисунок",
    illustration: "Иллюстрации",
    cover: "Иллюстрация обложки",
    original: "Автор оригинала",
    design: "Дизайн персонажей",
    translate: "Перевод"
  }
end
