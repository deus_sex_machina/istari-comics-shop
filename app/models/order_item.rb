class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product
  belongs_to :preorder

  validates :product, presence: true
  validates :count,
    presence: true,
    numericality: {
      only_integer: true,
      less_than_or_equal_to: 10
    }

    def display_name
      "[#{self.product.category.name}] #{self.product.title} × #{self.count}"
    end

  # Returns true if all products are available
  def available?
    product.available?(count)
  end

  # Returns true if product is in preorder now
  def in_preorder_now?
    product.in_preorder_now?
  end

  # Returns true if product is in preorder now
  # or all products are available
  def available_or_in_preorder_now?
    in_preorder_now? || available?
  end

  # Returns total price
  def price
    product.price * count
  end

  # Returns total weight
  def weight
    product.weight * count
  end

  # Saves preorder state
  def save_preorder_state!
    update!(preorder: product.preorder)
  end

  # Reserves all products from stock
  def reserve!
    product.reserve!(count)
  end

  # Frees products
  def free!
    product.free!(count)
  end
end
