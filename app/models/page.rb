class Page < ActiveRecord::Base
  validates :title, presence: true
  validates :url, presence: true, uniqueness: true

  def to_param
    "#{id}-#{url}"
  end
end
