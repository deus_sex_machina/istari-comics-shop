class ProductImage < ActiveRecord::Base
  belongs_to :product

  validates :img, presence: true

  mount_uploader :img, ProductImageUploader
end
