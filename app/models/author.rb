class Author < ActiveRecord::Base
  has_many :author_product_maps
  has_many :products, through: :author_product_maps

  validates :full_name, presence: true
  def display_name
    full_name
  end
end
