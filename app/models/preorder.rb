class Preorder < ActiveRecord::Base
  has_many :products

  validates :start, presence: true
  validates :finish, presence: true
  validate :must_start_before_its_finish

  scope :actual, -> { where(":now <= preorders.finish", now: Time.zone.now) }

  def current?
    now = Time.zone.now
    now >= start && now < finish
  end

  def upcoming?
    now = Time.zone.now
    now <= start
  end

  private

  def must_start_before_its_finish
    message = " должно быть раньше конца предзаказа"
    errors.add(:start, message) if finish <= start
  end
end
