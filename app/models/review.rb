class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :product

  validates :user, presence: true
  validates :text, presence: true
  validates :product, presence: true

  scope :pending, -> { where(status: :pending) }
  scope :confirmed, -> { where(status: :confirmed) }

  enum status: {
    pending: "pending",
    confirmed: "confirmed",
    rejected: "rejected"
  }

  def user_name
    "#{user.first_name} #{user.last_name}" 
  end

  def as_json(options = { })
    h = super(options)

    h["name"] = user_name
    h.delete("user_id")

    h["product-id"] = h["product_id"]
    h.delete("product_id")

    return h
  end
end
