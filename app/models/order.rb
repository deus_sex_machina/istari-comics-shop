class Order < ActiveRecord::Base
  has_many :order_items
  belongs_to :user

  enum status: {
    canceled: "canceled",
    created: "created",
    processed: "processed",
    paid: "paid",
    stocked: "stocked",
    packed: "packed",
    shipped: "shipped",
    delivered: "delivered"
  }

  enum delivery_type: {
    post: "post",
    checkout: "checkout"
  }

  before_save :process_if_should!
  before_save :pay_if_should!
  before_save :reserve_if_should!
  before_save :pack_if_should!
  before_destroy :free_if_should!

  # Returns true if all items are available
  def available?
    order_items.all?(&:available?)
  end

  # Returns true if any product is in preorder now
  def in_preorder_now?
    order_items.any?(&:in_preorder_now?)
  end

  # Returns true if all items are
  # in preorder now or available
  def available_or_in_preorder_now?
    order_items.all?(&:available_or_in_preorder_now?)
  end

  # Returns true if there is related delivery
  # def delivery?
  #   !delivery.nil?
  # end

  # Returns pure (without delivery) price
  def pure_price
    order_items.map(&:price).sum
  end

  # Returns pure (without packing) weight
  def pure_weight
    order_items.map(&:weight).sum
  end

  # Saves preorder state
  def save_preorder_state!
    order_items.each(&:save_preorder_state!)
  end

  # Frees products
  def free!
    order_items.each(&:free!)
  end

  # Reserves all products from stock
  def reserve!
    order_items.each(&:reserve!)
  end

  private

  # Returns true if status should be changed to :processed
  def should_change_status_to_processed?
    created? && delivery?
  end

  # Changes status to :processed if should
  def process_if_should!
    processed! if should_change_status_to_processed?
  end

  # Returns true if status should be changed to :paid
  def should_change_status_to_paid?
    processed? && paid >= price
  end

  # Changes status to :paid and save preorders state if should
  def pay_if_should!
    if should_change_status_to_paid?
      save_preorder_state!
      paid!
    end
  end

  # Returns true if status should be changed to :stocked
  def should_change_status_to_stocked?
    paid? && available?
  end

  # Changes status to :stocked and reserve prodcuts if should
  def reserve_if_should!
    if should_change_status_to_stocked?
      reserve!
      stocked!
    end
  end

  # Returns true if shipping is approved
  def should_be_shipped?
    !defer_shipping?
  end

  # Returns true if preorder is awating and finished
  # or not awaited
  def does_not_preorder_prevent_shipping?
    !await_preorder? ||
    (await_preorder? && !in_preorder_now?)
  end

  # Returns true if status should be changed to :packed
  def should_change_status_to_packed?
    stocked? &&
    should_be_shipped? &&
    does_not_preorder_prevent_shipping?
  end

  # Changes status to :packed if should
  def pack_if_should!
    packed! if should_change_status_to_packed?
  end

  # Returns true if products should be freed up
  def should_products_be_freed_up?
    stocked? || packed? || shipped? || delivered?
  end

  # Frees products before destroy if should
  def free_if_should!
    free! if should_products_be_freed_up?
  end
end
