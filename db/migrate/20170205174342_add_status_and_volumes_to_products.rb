class AddStatusAndVolumesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :status, :string, default: "ongoing"
    add_column :products, :russian_volumes, :integer
    add_column :products, :original_volumes, :integer
  end
end
