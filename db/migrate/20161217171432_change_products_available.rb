class ChangeProductsAvailable < ActiveRecord::Migration
  def change
    remove_column :products, :available
  end
end
