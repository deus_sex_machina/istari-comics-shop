class AddFlagsToDeliveries < ActiveRecord::Migration
  def change
    add_column :deliveries, :defer_shipping, :boolean, null: false, default: false
    add_column :deliveries, :await_preorder, :boolean, null: false, default: true
  end
end
