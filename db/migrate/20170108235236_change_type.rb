class ChangeType < ActiveRecord::Migration
  def change
    remove_column :deliveries, :type
    add_column :deliveries, :delivery_type, :string, default: "post"
  end
end
