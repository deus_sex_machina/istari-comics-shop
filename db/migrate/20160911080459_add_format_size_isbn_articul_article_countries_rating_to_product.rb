class AddFormatSizeIsbnArticulArticleCountriesRatingToProduct < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.text :format
      t.string :size
      t.string :isbn
      t.string :article
      t.string :countries
      t.string :rating
    end
  end
end
