class ChangeAvailableInProducts < ActiveRecord::Migration
  def change
    remove_column :orders, :items_available
  end
end
