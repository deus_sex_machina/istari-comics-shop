class RemoveCountriesFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :countries, :string
  end
end