class CreateCountriesProducts < ActiveRecord::Migration
  def change
    create_table :countries_products do |t|
      t.integer :country_id, index: true
      t.integer :product_id, index: true
    end
  end
end