class CreateAuthorProductMap < ActiveRecord::Migration
  def change
    create_table :author_product_maps do |t|
      t.integer :product_id
      t.integer :author_id
      t.string :role, default: "Автор"
    end
  end
end
