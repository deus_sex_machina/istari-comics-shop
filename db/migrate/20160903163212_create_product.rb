class CreateProduct < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.integer :pages
      t.string :cover
      t.integer :weight
      t.integer :price
      t.integer :series_id
      t.integer :category_id
    end
  end
end
