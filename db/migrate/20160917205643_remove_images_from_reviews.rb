class RemoveImagesFromReviews < ActiveRecord::Migration
  def change
    remove_column :reviews, :images, :string
  end
end
