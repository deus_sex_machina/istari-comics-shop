class AddWeightAndPriceToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :weight, :integer
    add_column :orders, :price, :integer
  end
end
