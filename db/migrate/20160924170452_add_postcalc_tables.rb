class AddPostcalcTables < ActiveRecord::Migration
  def up
    execute "CREATE TABLE postcalc_light_cities (city varchar(100) NOT NULL DEFAULT '',pindex varchar(6) NOT NULL DEFAULT '',PRIMARY KEY (city,pindex));"
    execute "CREATE TABLE postcalc_light_post_indexes (pindex varchar(6),opsname varchar(55) NOT NULL DEFAULT '',PRIMARY KEY (pindex,opsname));"
    execute "CREATE TABLE postcalc_light_countries (iso2 varchar(2),country varchar(40) NOT NULL DEFAULT '',PRIMARY KEY (iso2,country));"
  end

  def down
    drop_table :postcalc_light_cities
    drop_table :postcalc_light_post_indexes
    drop_table :postcalc_light_countries
  end
end
