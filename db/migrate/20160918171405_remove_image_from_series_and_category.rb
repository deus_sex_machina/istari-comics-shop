class RemoveImageFromSeriesAndCategory < ActiveRecord::Migration
  def change
    remove_column :series, :img, :string
    remove_column :authors, :img, :string
  end
end
