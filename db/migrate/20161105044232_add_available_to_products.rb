class AddAvailableToProducts < ActiveRecord::Migration
  def change
    add_column :products, :available, :integer, null: false, default: 0
  end
end
