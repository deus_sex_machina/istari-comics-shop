class ChangePreordersAndProducts < ActiveRecord::Migration
  def change
    remove_column :products, :preorder_start
    remove_column :products, :preorder_end
    add_column :preorders, :preorder_start, 'timestamp with time zone'
    add_column :preorders, :preorder_end, 'timestamp with time zone'
  end
end
