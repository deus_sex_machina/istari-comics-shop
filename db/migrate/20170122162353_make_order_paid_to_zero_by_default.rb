class MakeOrderPaidToZeroByDefault < ActiveRecord::Migration
  def change
    change_column :orders, :paid,  :integer, null: false, default: 0
    change_column :orders, :price, :integer, null: false, default: 0
  end
end
