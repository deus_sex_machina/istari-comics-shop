class AddPreviewsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :preview_pdf, :string
    add_column :products, :preview_cbr, :string
    add_column :products, :youtube_url, :string
  end
end
