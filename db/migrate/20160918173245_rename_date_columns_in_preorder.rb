class RenameDateColumnsInPreorder < ActiveRecord::Migration
  def change
    change_table :preorders do |t|
      t.rename :preorder_start, :start
      t.rename :preorder_end, :finish
    end
  end
end
