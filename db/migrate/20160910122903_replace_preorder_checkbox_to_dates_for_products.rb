class ReplacePreorderCheckboxToDatesForProducts < ActiveRecord::Migration
  def up
    change_table :products do |t|
      t.remove :preorder
      t.datetime :preorder_start, :preorder_end
    end
  end

  def down
    change_table :products do |t|
      t.boolean :preorder
      t.remove :preorder_start, :preorder_end
    end
  end
end
