class CreatePreorder < ActiveRecord::Migration
  def change
    create_table :preorders do |t|
      t.string :name
      t.text :description
    end
  end
end
