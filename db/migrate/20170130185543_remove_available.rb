class RemoveAvailable < ActiveRecord::Migration
  def up
    remove_column :orders, :items_available
    remove_column :order_items, :available
  end

  def down
    add_column :orders, :items_available, :boolean, default: false
    add_column :order_items, :items_available, :boolean, default: false
  end
end
