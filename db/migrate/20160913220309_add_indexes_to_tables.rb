class AddIndexesToTables < ActiveRecord::Migration
  def change
    add_index :features, :page_id
    add_index :reviews, :user_id
  end
end
