class AddCreatedAtToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :created_at, :datetime
  end
end
