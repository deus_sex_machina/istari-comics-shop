class CreateDelivery < ActiveRecord::Migration
  def change
    create_table :deliveries do |t|
      t.string :type, default: "post"
      t.integer :external_id
      t.integer :order_id
      t.string :comment
      t.integer :cost
      t.string :country
      t.string :address
    end
  end
end
