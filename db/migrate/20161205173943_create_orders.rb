class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id, index: true
      t.string :status, default: "created"
      t.boolean :items_available, default: false
    end
    create_table :order_items do |t|
      t.integer :order_id, index: true
      t.integer :product_id, index: true
      t.integer :preorder_id, index: true
      t.integer :count
      t.boolean :available, default: false
    end
  end
end
