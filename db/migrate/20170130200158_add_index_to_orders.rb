class AddIndexToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :index, :string
  end
end
