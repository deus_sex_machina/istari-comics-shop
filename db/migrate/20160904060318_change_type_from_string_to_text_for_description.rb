class ChangeTypeFromStringToTextForDescription < ActiveRecord::Migration
  def up
    change_column :authors, :description, :text
    change_column :features, :description, :text
    change_column :series, :description, :text
  end

  def down
    change_column :authors, :description, :string
    change_column :features, :description, :string
    change_column :series, :description, :string
  end
end
