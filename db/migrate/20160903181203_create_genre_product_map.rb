class CreateGenreProductMap < ActiveRecord::Migration
  def change
    create_table :genre_product_maps do |t|
      t.integer :product_id
      t.integer :genre_id
    end
  end
end
