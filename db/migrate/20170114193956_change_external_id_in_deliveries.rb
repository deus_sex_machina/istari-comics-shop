class ChangeExternalIdInDeliveries < ActiveRecord::Migration
  def up
    remove_column :deliveries, :external_id
    add_column :deliveries, :external_id, :string, default: ""
  end

  def down
    remove_column :deliveries, :external_id
    add_column :deliveries, :external_id, :integer
  end
end
