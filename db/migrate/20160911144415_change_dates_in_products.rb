class ChangeDatesInProducts < ActiveRecord::Migration
  def change
    remove_column :products, :preorder_start
    remove_column :products, :preorder_end
    add_column :products, :preorder_start, 'timestamp with time zone'
    add_column :products, :preorder_end, 'timestamp with time zone'
  end
end
