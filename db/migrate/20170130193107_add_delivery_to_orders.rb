class AddDeliveryToOrders < ActiveRecord::Migration
  def up
    drop_table :deliveries
    add_column :orders, :delivery, :boolean
    add_column :orders, :comment, :string
    add_column :orders, :cost, :integer
    add_column :orders, :country, :string
    add_column :orders, :address, :string
    add_column :orders, :delivery_type, :string, default: "post"
    add_column :orders, :external_id, :string, default: ""
    add_column :orders, :full_name, :string
    add_column :orders, :defer_shipping, :boolean, default: false,  null: false
    add_column :orders, :await_preorder, :boolean, default: true,  null: false
  end

  def down
    remove_column :orders, :delivery
    remove_column :orders, :comment
    remove_column :orders, :cost
    remove_column :orders, :country
    remove_column :orders, :address
    remove_column :orders, :delivery_type
    remove_column :orders, :external_id
    remove_column :orders, :full_name
    remove_column :orders, :defer_shipping
    remove_column :orders, :await_preorder

    create_table "deliveries", force: :cascade do |t|
      t.integer "order_id"
      t.string  "comment"
      t.integer "cost"
      t.string  "country"
      t.string  "address"
      t.string  "delivery_type",  default: "post"
      t.string  "external_id",    default: ""
      t.string  "full_name"
      t.boolean "defer_shipping", default: false,  null: false
      t.boolean "await_preorder", default: true,   null: false
    end
  end
end
