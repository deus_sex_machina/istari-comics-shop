class AddFullNameToDeliveries < ActiveRecord::Migration
  def up
    remove_column :deliveries, :first_name
    remove_column :deliveries, :last_name
    remove_column :deliveries, :middle_name
    add_column :deliveries, :full_name, :string
  end

  def down
    add_column :deliveries, :full_name, :string
    add_column :deliveries, :first_name, :string
    add_column :deliveries, :last_name, :string
    add_column :deliveries, :middle_name, :string
  end
end
