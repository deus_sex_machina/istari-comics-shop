class CreateSocialNetworks < ActiveRecord::Migration
  def change
    create_table :social_networks do |t|
      t.string :icon
      t.string :name
      t.string :link
    end
  end
end
