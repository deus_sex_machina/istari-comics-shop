class AddPreorderToProducts < ActiveRecord::Migration
  def change
    add_column :products, :preorder, :boolean, default: false
  end
end
