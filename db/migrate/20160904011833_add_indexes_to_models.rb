class AddIndexesToModels < ActiveRecord::Migration
  def change
    add_index :author_product_maps, :product_id
    add_index :author_product_maps, :author_id

    add_index :genre_product_maps, :product_id
    add_index :genre_product_maps, :genre_id

    add_index :products, :series_id
    add_index :products, :category_id
  end
end
