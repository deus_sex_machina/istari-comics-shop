class AddPreorderIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :preorder_id, :integer
  end
end
