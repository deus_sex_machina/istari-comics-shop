class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :user_id
      t.text :text
      t.string :images,  default: [], array: true
    end
  end
end
