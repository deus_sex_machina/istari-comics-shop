class AddFioToDeliveries < ActiveRecord::Migration
  def change
    add_column :deliveries, :first_name, :string
    add_column :deliveries, :middle_name, :string
    add_column :deliveries, :last_name, :string
  end
end
