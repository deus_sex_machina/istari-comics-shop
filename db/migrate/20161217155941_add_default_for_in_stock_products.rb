class AddDefaultForInStockProducts < ActiveRecord::Migration
  def change
    remove_column :products, :in_stock
    add_column :products, :in_stock, :integer, default: 0
  end
end
