class AddDefaultValueToDelivery < ActiveRecord::Migration
  def change
    change_column :orders, :delivery, :boolean, null: false, default: false
  end
end
