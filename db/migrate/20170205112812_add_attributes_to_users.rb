class AddAttributesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :phone, :string
    add_column :users, :address, :string
    add_column :users, :index, :string
    add_column :users, :locality, :string
  end
end
