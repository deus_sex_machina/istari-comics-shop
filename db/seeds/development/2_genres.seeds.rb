Genre.delete_all

genres = [
  { name: "приключения" },
  { name: "ммо" },
  { name: "романтика" },
  { name: "фэнтези" },
  { name: "сёнэн" },
  { name: "cэйнэн" },
]

genres.each_with_index do |genre, i|
  item = Genre.create!(genre)
  puts "Create: Genre for #{item.name}, #{i + 1}/#{genres.size}"
end
