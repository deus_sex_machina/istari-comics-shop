Author.delete_all

authors = [
  { full_name: "Рэки Кавахара", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
  { full_name: "abec", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
  { full_name: "Кейто Коумэ", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
  { full_name: "Исуна Хасэкура", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
  { full_name: "Корэ Ямадзаки", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
  { full_name: "К. Киц", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
  { full_name: "О. Бугуцкий", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
  { full_name: "Ushwood", description: Faker::Lorem.paragraphs(rand(4..6)).join("\n") },
]

authors.each_with_index do |author, i|
  item = Author.create!(author)
  puts "Create: Author for #{item.full_name}, #{i + 1}/#{authors.size}"
end
