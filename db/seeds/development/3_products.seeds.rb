Product.delete_all

product_groups = [
  {
    series: "Волчица и пряности",
    category: "Манга",
    price: 300,
    genres: %w( приключения cэйнэн ),
    authors: [
      { name: "Кейто Коумэ", role: :art },
      { name: "Исуна Хасэкура", role: :story },
      { name: "О. Бугуцкий", role: :translate },
    ],
    covers: %w(
      http://istaricomics.com:8080/images/shop/1.jpg
      http://istaricomics.com:8080/images/shop/2.jpg
      http://istaricomics.com:8080/images/shop/3.jpg
      http://istaricomics.com:8080/images/shop/4.jpg
      http://istaricomics.com:8080/images/shop/5.jpg
      http://istaricomics.com:8080/images/shop/6.jpg
    ),
  },
  {
    series: "Невеста чародея",
    category: "Манга",
    price: 300,
    genres: %w( приключения фэнтези сёнэн ),
    authors: [
      { name: "Корэ Ямадзаки", role: :author },
      { name: "К. Киц", role: :translate },
    ],
    covers: %w(
      http://istaricomics.com:8080/images/shop/7.jpg
      http://istaricomics.com:8080/images/shop/8.jpg
      http://istaricomics.com:8080/images/shop/9.jpg
    ),
  },
  {
    series: "Sword Art Online",
    category: "Манга",
    price: 300,
    genres: %w( приключения ммо романтика сёнэн ),
    authors: [
      { name: "Рэки Кавахара", role: :story },
      { name: "abec", role: :illustration },
      { name: "Ushwood", role: :translate },
    ],
    covers: %w(
      http://istaricomics.com:8080/images/shop/10.jpg
      http://istaricomics.com:8080/images/shop/11.jpg
    ),
  },
  {
    series: "Волчица и пряности",
    category: "Ранобэ",
    preorder: "SAW",
    price: 500,
    genres: %w( приключения cэйнэн ),
    authors: [
      { name: "Кейто Коумэ", role: :art },
      { name: "Исуна Хасэкура", role: :story },
      { name: "О. Бугуцкий", role: :translate },
    ],
    covers: %w(
      http://istaricomics.com:8080/images/shop/12.jpg
      http://istaricomics.com:8080/images/shop/13.jpg
      http://istaricomics.com:8080/images/shop/14.jpg
    ),
  },
  {
    series: "Sword Art Online",
    category: "Ранобэ",
    preorder: "SAO",
    price: 500,
    genres: %w( приключения ммо романтика сёнэн ),
    authors: [
      { name: "Рэки Кавахара", role: :story },
      { name: "abec", role: :illustration },
      { name: "Ushwood", role: :translate },
    ],
    covers: %w(
      http://istaricomics.com:8080/images/shop/15.jpg
      http://istaricomics.com:8080/images/shop/16.jpg
      http://istaricomics.com:8080/images/shop/17.jpg
      http://istaricomics.com:8080/images/shop/18.jpg
    ),
  },
]

product_groups_size = product_groups.map { |g| g[:covers].size }.sum
index = 0

product_groups.each do |g|
  template = {
    series: Series.find_by(name: g[:series]),
    category: Category.find_by(name: g[:category]),
    genres: g[:genres].map { |g| Genre.find_by(name: g) },
    authors: g[:authors].map { |a| Author.find_by(full_name: a[:name]) },
    price: g[:price],
    preorder: Preorder.find_by(name: g[:preorder])
  }

  vol = 0
  g[:covers].each do |cover|
    index += 1
    vol += 1
    data = template.clone
    data[:remote_cover_url] = cover
    data[:title] = "#{data[:series].name}. Том #{vol}"
    data[:description] = Faker::Lorem.paragraphs(rand(2..4)).join("\n")
    data[:weight] = 100
    data[:pages] = 200
    data[:isbn] = "978-5-904676-65-0"
    data[:article] = "TEST"
    data[:countries] = [Country.find_by(name: "Япония")]
    data[:rating] = "12+"
    data[:in_stock] = rand(10)

    if g[:category] == "Ранобэ"
      data[:format] = "Глянцевая суперобложка; твердый переплёт; внутренний блок: цветные и ч/б иллюстрации, ч/б текст"
      data[:size] = "142x215x20"
    else
      data[:format] = "Матовая суперобложка, мягкий переплёт, внутренний блок: ч/б"
      data[:size] = "128x182x15"
    end

    item = Product.create!(data)

    item.author_product_maps.each_with_index do |apm, i|
      apm.role = g[:authors][i][:role]
      apm.save!
    end

    item.save!

    puts "Create: Product for #{item.title}, #{index}/#{product_groups_size}"
  end
end
