SocialNetwork.delete_all

sns = [
  {
    icon: "vk",
    name: "ВК-группа издательства",
    link: "//vk.com/istaricomics",
  }, {
    icon: "instagram",
    name: "Инстаграмм издательства",
    link: "//www.instagram.com/istari_comics/",
  }, {
    icon: "twitter",
    name: "Твиттер издательства",
    link: "//twitter.com/istaricomics",
  }, {
    icon: "youtube",
    name: "YouTube-канал издательства",
    link: "//www.youtube.com/channel/UCfhpJMb85FqE4o7xxRdDXDg"
  },
]

sns.each_with_index do |sn, i|
  item = SocialNetwork.create!(sn)
  puts "Create: SocialNetwork for #{item.name}, #{i + 1}/#{sns.size}"
end
