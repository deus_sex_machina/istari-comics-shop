Feature.delete_all

features = [
  {
    title: "Предзаказ",
    icon: "ticket",
    description: Faker::Lorem.paragraph,
    page: "preorder",
  }, {
    title: "Доставка",
    icon: "truck",
    description: Faker::Lorem.paragraph,
    page: "delivery",
  }, {
    title: "Оплата",
    icon: "credit-card",
    description: Faker::Lorem.paragraph,
    page: "payment",
  }, {
    title: "Возврат товара",
    icon: "signing",
    description: Faker::Lorem.paragraph,
    page: "purchase-returns",
  },
]

features.each_with_index do |feature, i|
  feature[:page] = Page.find_by(url: feature[:page])
  item = Feature.create!(feature)
  puts "Create: Feature for #{item.title}, #{i + 1}/#{features.size}"
end
