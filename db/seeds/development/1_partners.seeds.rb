Partner.delete_all

partners = [
  {
    name: "Кадокава Сётэн",
    logo: "http://i.imgur.com/ZqeGZ9G.png",
    link: "//shoten.kadokawa.co.jp",
  }, {
    name: "Медиа Фактори",
    logo: "http://i.imgur.com/hxZZ8ST.png",
    link: "//www.mediafactory.co.jp",
  }, {
    name: "Aски Медиа Воркс",
    logo: "http://i.imgur.com/lO5xvRM.png",
    link: "//asciimw.jp",
  }, {
    name: "Энтэрбрaйн",
    logo: "http://i.imgur.com/5BQHKTM.png",
    link: "//www.enterbrain.co.jp",
  }, {
    name: "Хакусэнся",
    logo: "http://i.imgur.com/xnIODSk.png",
    link: "//www.hakusensha.co.jp",
  }, {
    name: "Кадокава Корпорейшн",
    logo: "http://i.imgur.com/ndjS3ut.png",
    link: "//www.kadokawa.co.jp",
  }, {
    name: "Кодaнся",
    logo: "http://i.imgur.com/ZiwZU3O.png",
    link: "//www.kodansha.co.jp",
  }, {
    name: "Мэг Гарден",
    logo: "http://i.imgur.com/GwNtigz.png",
    link: "//www.mag-garden.co.jp",
  }, {
    name: "Хаксан",
    logo: "http://i.imgur.com/fxF60t4.png",
    link: "//www.haksanpub.co.kr",
  }, {
    name: "Тонг Ли",
    logo: "http://i.imgur.com/bUo1DYe.png",
    link: "//www.tongli.com.tw",
  }, {
    name: "Seoul Cultural",
    logo: "http://i.imgur.com/Pdcq8Gf.png",
    link: "//www.smlounge.co.kr"
  }, {
    name: "Бунгэи сюндзю",
    logo: "http://i.imgur.com/x0AWLZE.png",
    link: "www.bunshun.co.jp",
  },
]

partners.each_with_index do |partner, i|
  partner[:remote_logo_url] = partner[:logo]
  item = Partner.create!(partner)
  puts "Create: Partner for #{item.name}, #{i + 1}/#{partners.size}"
end
