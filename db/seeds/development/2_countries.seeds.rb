Country.delete_all

countries = [
  { name: "Япония", description: Faker::Lorem.paragraphs(rand(2..3)).join("\n") },
]

countries.each_with_index do |country, i|
  item = Country.create!(country)
  puts "Create: Country for #{item.name}, #{i + 1}/#{countries.size}"
end
