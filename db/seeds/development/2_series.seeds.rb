Series.delete_all

series = [
  { name: "Волчица и пряности", description: Faker::Lorem.paragraphs(rand(2..3)).join("\n") },
  { name: "Невеста чародея",    description: Faker::Lorem.paragraphs(rand(2..3)).join("\n") },
  { name: "Sword Art Online",   description: Faker::Lorem.paragraphs(rand(2..3)).join("\n") },
]

series.each_with_index do |single_series, i|
  item = Series.create!(single_series)
  puts "Create: Series for #{item.name}, #{i + 1}/#{series.size}"
end
