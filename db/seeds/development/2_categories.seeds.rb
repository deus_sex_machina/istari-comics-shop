Category.delete_all

categories = [
  { name: "Манга", description: Faker::Lorem.paragraphs(rand(2..3)).join("\n") },
  { name: "Ранобэ", description: Faker::Lorem.paragraphs(rand(2..3)).join("\n") },
  { name: "Артбуки", description: Faker::Lorem.paragraphs(rand(2..3)).join("\n") },
]

categories.each_with_index do |category, i|
  item = Category.create!(category)
  puts "Create: Category for #{item.name}, #{i + 1}/#{categories.size}"
end
