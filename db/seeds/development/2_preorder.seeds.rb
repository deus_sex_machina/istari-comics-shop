Preorder.delete_all

now = Time.zone.now

preorders = [
  { name: "SAO", start: now, finish: now + 30.days },
  { name: "SAW", start: now + 10.days, finish: now + 50.days },
]

preorders.each_with_index do |preorder, i|
  item = Preorder.create!(preorder)
  puts "Create: Preorder for #{item.name}, #{i + 1}/#{preorders.size}"
end
