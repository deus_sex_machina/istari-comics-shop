Slide.delete_all

slides = [
  {
    title: "Edward",
    img: "http://orig15.deviantart.net/f8b2/f/2015/251/3/0/flat_anime_character__edward_elric_by_xtr3m0nst3r-d98u0ux.jpg",
  }, {
    title: "Saitama",
    img: "http://orig04.deviantart.net/e642/f/2015/304/8/7/saitama__one_punchman__wallpaper_minimalist_anime_by_lucifer012-d9f3onr.png",
  }, {
    title: "someone from Naruto",
    img: "http://orig12.deviantart.net/6fb6/f/2015/213/e/b/mitsuki__naruto_gaiden__minimalist_anime_wallpaper_by_lucifer012-d93poo0.png",
  }, {
    title: "Ken",
    img: "https://lh3.googleusercontent.com/-4EgY6ZAa8eQ/VlDvjTBy8dI/AAAAAAAAsVE/PhcmfiwfRtQ/w1920-h1080/ken_kaneki_mask__tokyo_ghoul__minimalist_wallpaper_by_greenmapple17-d92i44d.png",
  },
]

slides.each_with_index do |slide, i|
  slide[:remote_img_url] = slide[:img]
  item = Slide.create!(slide)
  puts "Create: Slide for #{item.title}, #{i + 1}/#{slides.size}"
end
