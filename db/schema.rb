# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170205183446) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",               default: "", null: false
    t.string   "encrypted_password",  default: "", null: false
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",       default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree

  create_table "author_product_maps", force: :cascade do |t|
    t.integer "product_id"
    t.integer "author_id"
    t.string  "role",       default: "Автор"
  end

  add_index "author_product_maps", ["author_id"], name: "index_author_product_maps_on_author_id", using: :btree
  add_index "author_product_maps", ["product_id"], name: "index_author_product_maps_on_product_id", using: :btree

  create_table "authors", force: :cascade do |t|
    t.string "full_name"
    t.text   "description"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.text   "description"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string "name"
    t.text   "description"
  end

  create_table "countries_products", force: :cascade do |t|
    t.integer "country_id"
    t.integer "product_id"
  end

  add_index "countries_products", ["country_id"], name: "index_countries_products_on_country_id", using: :btree
  add_index "countries_products", ["product_id"], name: "index_countries_products_on_product_id", using: :btree

  create_table "features", force: :cascade do |t|
    t.string  "title"
    t.text    "description"
    t.string  "icon"
    t.integer "page_id"
  end

  add_index "features", ["page_id"], name: "index_features_on_page_id", using: :btree

  create_table "genre_product_maps", force: :cascade do |t|
    t.integer "product_id"
    t.integer "genre_id"
  end

  add_index "genre_product_maps", ["genre_id"], name: "index_genre_product_maps_on_genre_id", using: :btree
  add_index "genre_product_maps", ["product_id"], name: "index_genre_product_maps_on_product_id", using: :btree

  create_table "genres", force: :cascade do |t|
    t.string "name"
  end

  create_table "order_items", force: :cascade do |t|
    t.integer "order_id"
    t.integer "product_id"
    t.integer "preorder_id"
    t.integer "count"
  end

  add_index "order_items", ["order_id"], name: "index_order_items_on_order_id", using: :btree
  add_index "order_items", ["preorder_id"], name: "index_order_items_on_preorder_id", using: :btree
  add_index "order_items", ["product_id"], name: "index_order_items_on_product_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "status",         default: "created"
    t.integer  "weight"
    t.integer  "price",          default: 0,         null: false
    t.integer  "paid",           default: 0,         null: false
    t.boolean  "delivery",       default: false,     null: false
    t.string   "comment"
    t.integer  "cost"
    t.string   "country"
    t.string   "address"
    t.string   "delivery_type",  default: "post"
    t.string   "external_id",    default: ""
    t.string   "full_name"
    t.boolean  "defer_shipping", default: false,     null: false
    t.boolean  "await_preorder", default: true,      null: false
    t.string   "index"
    t.datetime "created_at"
  end

  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "packing_costs", force: :cascade do |t|
    t.integer "cost"
  end

  create_table "pages", force: :cascade do |t|
    t.string "title"
    t.text   "content"
    t.string "url"
  end

  create_table "partners", force: :cascade do |t|
    t.string "name"
    t.string "logo"
    t.string "link"
  end

  create_table "postcalc_light_cities", id: false, force: :cascade do |t|
    t.string "city",   limit: 100, default: "", null: false
    t.string "pindex", limit: 6,   default: "", null: false
  end

  create_table "postcalc_light_countries", id: false, force: :cascade do |t|
    t.string "iso2",    limit: 2,               null: false
    t.string "country", limit: 40, default: "", null: false
  end

  create_table "postcalc_light_post_indexes", id: false, force: :cascade do |t|
    t.string "pindex",  limit: 6,               null: false
    t.string "opsname", limit: 55, default: "", null: false
  end

  create_table "preorders", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "start"
    t.datetime "finish"
  end

  create_table "product_images", force: :cascade do |t|
    t.string  "img"
    t.integer "product_id"
  end

  add_index "product_images", ["product_id"], name: "index_product_images_on_product_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.string  "title"
    t.integer "pages"
    t.string  "cover"
    t.integer "weight"
    t.integer "price"
    t.integer "series_id"
    t.integer "category_id"
    t.text    "description"
    t.text    "format"
    t.string  "size"
    t.string  "isbn"
    t.string  "article"
    t.string  "rating"
    t.integer "preorder_id"
    t.integer "in_stock",         default: 0
    t.string  "preview_pdf"
    t.string  "preview_cbr"
    t.string  "status",           default: "ongoing"
    t.integer "russian_volumes"
    t.integer "original_volumes"
  end

  add_index "products", ["category_id"], name: "index_products_on_category_id", using: :btree
  add_index "products", ["preorder_id"], name: "index_products_on_preorder_id", using: :btree
  add_index "products", ["series_id"], name: "index_products_on_series_id", using: :btree

  create_table "reviews", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "text"
    t.string   "status",     default: "pending"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
  end

  add_index "reviews", ["product_id"], name: "index_reviews_on_product_id", using: :btree
  add_index "reviews", ["user_id"], name: "index_reviews_on_user_id", using: :btree

  create_table "series", force: :cascade do |t|
    t.string "name"
    t.text   "description"
  end

  create_table "slides", force: :cascade do |t|
    t.string "link"
    t.string "img"
    t.string "title"
  end

  create_table "social_networks", force: :cascade do |t|
    t.string "icon"
    t.string "name"
    t.string "link"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.json     "tokens"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "address"
    t.string   "index"
    t.string   "locality"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
