(ns app.db
  (:require [plumbing.core :refer [defnk map-from-vals fn->>]]
            [clojure.set :refer [rename-keys]]))

(def default-db
  {:fetchers 0
   :page [:nopage]
   :user nil
   :categories {}
   :series {}
   :products {}
   :cart {}
   :orders {}
   :notifications {}
   :access-data {:access-token (.getItem js/localStorage :access-token)
                 :client       (.getItem js/localStorage :client)
                 :uid          (.getItem js/localStorage :uid)}
   :notifications-next-id 1})

(defn collection->clj
  ([data ->clj] (collection->clj data ->clj :id))
  ([data ->clj key] (->> data (map ->clj) (map-from-vals key))))

(defn category->clj [data]
  (select-keys data [:id :name :description]))

(defn series->clj [data]
  (select-keys data [:id :name :description]))

(defn page->clj [data]
  (select-keys data [:id :title :url :content]))

(defn product->clj [data]
  (select-keys data [:id :title :pages :weight :description
                     :article :rating :format :size :isbn
                     :price :available :category-id :series-id
                     :preorder-id :cover-url :cover-thumb
                     :reviews]))

(defn feature->clj [data]
  (select-keys data [:id :title :description :icon :page-id
                     :series-id :cover-url :cover-thumb]))

(defn order-item->clj [data]
  (-> data (select-keys [:id :product_id :preorder_id
                         :count :price :weight :available])
           (rename-keys {:available :available?
                         :product_id :product-id
                         :preorder_id :preorder-id})))

(defn order->clj [data]
  (-> data
      (select-keys [:id :status :order_items :weight :price])
      (rename-keys {:order_items :order-items})
      (update :status keyword)
      (update :order-items (fn->> (map order-item->clj) doall))))
