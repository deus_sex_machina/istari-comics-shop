(ns app.core
  (:require [reagent.core :refer [render] :refer-macros [with-let]]
            [re-frame.core :refer [dispatch-sync dispatch]]
            [app.views.core :as views]
            [app.events.core]
            [app.subs]
            [app.routes :refer [route-app]]))


(defn mount-root []
  (let [root (.getElementById js/document "root")]
    (render [views/app] root)))

(defn init []
  (enable-console-print!)
  (dispatch-sync [:init])
  (route-app)
  (.addEventListener js/window "storage" #(dispatch [:uncache-cart])))

(defn run []
  (mount-root))
