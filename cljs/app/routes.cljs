(ns app.routes
    (:import goog.History)
    (:require [re-frame.core :refer [dispatch]]
              [secretary.core :as secretary :refer-macros [defroute]]
              [accountant.core :as accountant]
              [goog.events :as events]
              [goog.history.EventType :as EventType]))

(defn hook-accountant! []
  (accountant/configure-navigation!
   {:nav-handler  (fn [e]
                    (let [body (.-body js/document)
                          $body (js/jQuery body)
                          animate (aget $body "animate")]
                      (secretary/dispatch! e)
                      (.call animate $body #js{:scrollTop 0} 500)))
    :path-exists? secretary/locate-route})
  (accountant/dispatch-current!))

(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn route-app []
  ; (secretary/set-config! :prefix "#")

  (defroute index-path "/" []
    (dispatch [:set-page [:index]]))

  (defroute sign-in-path "/sign-in" []
    (dispatch [:set-page [:sign-in]]))

  (defroute sign-up-path "/sign-up" []
    (dispatch [:set-page [:sign-up]]))

  (defroute page-path "/page/:url" [url]
    (dispatch [:set-page [:page url]]))

  (defroute product-path "/products/:id" [id]
    (dispatch [:set-page [:product (int id)]]))

  (defroute category-path "/categories/:id" [id]
    (dispatch [:set-page [:category (int id)]]))

  (defroute all-series-path "/series" []
    (dispatch [:set-page [:all-series]]))

  (defroute series-path "/series/:id" [id]
    (dispatch [:set-page [:series (int id)]]))

  (defroute cart-path "/my/cart" [id]
    (dispatch [:set-page [:cart]]))

  (defroute orders-path "/my/orders" []
    (dispatch [:set-page [:orders]]))

  (defroute order-process-path "/my/orders/:id/processing" [id]
    (dispatch [:set-page [:order-proccessing (int id)]]))

  (defroute order-pay-path "/my/orders/:id/payment" [id]
    (dispatch [:set-page [:order-payment (int id)]]))

  ; (hook-browser-navigation!)
  (hook-accountant!))
