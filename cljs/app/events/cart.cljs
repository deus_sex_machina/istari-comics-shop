(ns app.events.cart
  (:require [re-frame.core :refer [dispatch reg-event-db reg-event-fx trim-v path]]
            [app.events.utils :refer [cache uncache]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Uncache cart
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-db :uncache-cart
  [(uncache :cart {:path [:cart], :force? true, :before? true})]
  (fn [db _] db))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Set count
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-db :set-cart-count
  [trim-v (cache :cart {:path [:cart]})]
  (fn [db [id n]]
    (let [available (get-in db [:products id :available])
          n (-> n int (min available) (max 0))]
      (if (zero? n)
        (update db :cart dissoc id)
        (assoc-in db [:cart id] n)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Update count
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :update-cart-count
  [trim-v]
  (fn [{db :db} [id delta]]
    (let [delta (int delta)
          prev-count (get-in db [:cart id])]
      {:dispatch [:set-cart-count id (+ prev-count delta)]})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Delete
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :delete-from-cart
  [trim-v]
  (fn [{db :db} [id]]
    {:dispatch [:set-cart-count id 0]}))

(reg-event-db :clear-cart
  [(path :cart) (cache :cart {:path [:cart]})]
  (constantly {}))
