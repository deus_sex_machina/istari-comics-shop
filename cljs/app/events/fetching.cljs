(ns app.events.fetching
  (:require [re-frame.core :refer [dispatch reg-event-db reg-event-fx
                                   trim-v path ->interceptor]]
            [ajax.core :as ajax]
            [app.db :as db :refer [collection->clj category->clj series->clj
                                   product->clj feature->clj page->clj
                                   order->clj]]
            [app.events.utils :refer [start-fetching stop-fetching cache uncache
                                      send-token]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Categories
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :fetch-categories
  [(path :categories) (uncache :categories) start-fetching]
  (fn [_ _]
    {:http-xhrio {:method :get
                  :uri "/api/categories"
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:success-fetch-categories]
                  :on-failure [:notify "Ошибка при загрузке категорий"]}}))

(reg-event-db :success-fetch-categories
  [trim-v (path :categories) (cache :categories {:keys [:id :name]}) stop-fetching]
  (fn [_ [data]]
    (collection->clj data category->clj)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Series
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :fetch-series
  [(path :series) (uncache :series) start-fetching]
  (fn [_ _]
    {:http-xhrio {:method :get
                  :uri "/api/series"
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:success-fetch-series]
                  :on-failure [:notify "Ошибка при загрузке серий"]}}))

(reg-event-db :success-fetch-series
  [trim-v (path :series) (cache :series {:keys [:id :name]}) stop-fetching]
  (fn [_ [data]]
    (collection->clj data series->clj)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Products
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :fetch-products
  [(path :products) (uncache :products) start-fetching]
  (fn [_ _]
    {:http-xhrio {:method :get
                  :uri "/api/products"
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:success-fetch-products]
                  :on-failure [:notify "Ошибка при загрузке товаров"]}}))

(reg-event-db :success-fetch-products
  [trim-v
   (path :products)
   (cache :products {:keys [:id :title :available :price :weight :category-id
                            :series-id :cover-url :cover-thumb :article]})
   stop-fetching]
  (fn [_ [data]]
    (collection->clj data product->clj)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Features
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO FAIL
(reg-event-fx :fetch-features
  [(path :products) (uncache :products) start-fetching]
  (fn [_ _]
    {:http-xhrio {:method :get
                  :uri "/api/features"
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:success-fetch-features]}}))

(reg-event-db :success-fetch-features
  [trim-v (path :features) (cache :features) stop-fetching]
  (fn [_ [data]]
    (collection->clj data feature->clj)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Pages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO FAIL
(reg-event-fx :fetch-pages
  [(path :products) (uncache :products) start-fetching]
  (fn [_ _]
    {:http-xhrio {:method :get
                  :uri "/api/pages"
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:success-fetch-pages]}}))

(reg-event-db :success-fetch-pages
  [trim-v (path :pages) (cache :pages {:keys [:id :title :url]}) stop-fetching]
  (fn [_ [data]]
    (collection->clj data page->clj :url)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Socials
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO FAIL
(reg-event-fx :fetch-socials
  [(path :products) (uncache :products) start-fetching]
  (fn [_ _]
    {:http-xhrio {:method :get
                  :uri "/api/social_networks"
                  :response-format (ajax/json-response-format {:keywords? true})
                  :on-success [:success-fetch-socials]}}))

(reg-event-db :success-fetch-socials
  [trim-v (path :socials) (cache :socials) stop-fetching]
  (fn [_ [data]]
    (collection->clj data identity)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Orders
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO FAIL
(reg-event-fx :fetch-orders
  [(path :categories) (uncache :categories) start-fetching send-token]
  (fn [_ _]
    {:http {:method :get
            :url "/api/protected/users/current/orders"
            :on-success [:success-fetch-orders]
            :on-failure [:notify "Orders load error"]}}))

(reg-event-db :success-fetch-orders
  [trim-v (path :orders) stop-fetching]
  (fn [_ [data]]
    (collection->clj data order->clj)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Fetch checkout ticket
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :fetch-checkout-ticket
  [start-fetching]
  (fn [_ _]
    {:http {:method :post
            :url "/api/callbacks/orders/_/delivery/checkout/session"
            :on-success [:success-fetch-checkout-ticket]
            :on-failure [:notify  "Can't connect to checkout"]}}))

(reg-event-db :success-fetch-checkout-ticket
  [trim-v (path :checkout-token) stop-fetching]
  (fn [_ [{:keys [token]}]]
    token))

(reg-event-db :clear-checkout-token
  [(path :checkout-token)]
  (constantly nil))
