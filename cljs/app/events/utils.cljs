(ns app.events.utils
  (:require [cljs.reader :refer [read-string]]
            [clojure.string :refer [lower-case]]
            [re-frame.core :refer [dispatch ->interceptor]]
            [plumbing.core :refer [letk fn-> map-keys map-vals]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Parse response
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn read-response [response callback drop]
  (let [response (.-target response)
        body (if drop
               {}
               (-> response
                   .getResponseJson
                   (js->clj :keywordize-keys true)))
        headers (-> response
                    .getResponseHeaders
                    (js->clj :keywordize-keys true)
                    (->> (map-keys (fn-> name lower-case keyword))))]
    (dispatch (conj callback body headers))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Attach token to request
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def send-token
  (->interceptor :after
    (fn [cofx]
      (let [access-token (.getItem js/localStorage :access-token)
            client (.getItem js/localStorage :client)
            uid (.getItem js/localStorage :uid)]
        (update-in
          cofx [:effects :http :headers]
          assoc "Access-Token" access-token
                "Client" client
                "Uid" uid)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Save token from response
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def save-token
  (->interceptor :after
    (fn [context]
      (letk [headers (-> context :coeffects :event second)
             [access-token client uid] headers
             db (get-in context [:coeffects :db])]
        (-> context
          (assoc-in [:effects :save-token] [access-token client uid]))))))
          ; (assoc-in [:effects :db]
          ;   (assoc db :access-data {:access-token access-token
          ;                           :client client
          ;                           :uid uid})))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Inc fetching
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def start-fetching
  (->interceptor :after #(update-in % [:effects :dispatch-n] conj [:start-fetching])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Dec fetching
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def stop-fetching
  (->interceptor :after #(update-in % [:effects :dispatch-n] conj [:stop-fetching])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Comporess data
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn ls->clj [key]
  (some->> (.getItem js/localStorage key)
           ((aget js/LZString "decompress"))
           (read-string)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Get data from local storage
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn uncache
  ([key]
   (uncache key {:path [], :force? false, :before? false}))
  ([key {:keys [path force? before?] :or {path [], force? false, before? false}}]
   (->interceptor (if before? :before :after)
     (fn [cofx]
       (let [dest (get-in cofx [:coeffects :db])]
         (if-not (or force? (empty? dest))
           cofx
           (let [cached (ls->clj key)
                 final-path (-> [(if before? :coeffects :effects)]
                                (concat [:db] path))]
             (if (nil? cached)
               cofx
               (assoc-in cofx final-path cached)))))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Push data to local storage
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn cache
  ([key]
   (cache key {:path []}))
  ([key {:keys [path keys] :or {path []}}]
   (->interceptor :after
     (fn [{{data :db} :effects :as cofx}]
       (let [select-keys* (if (nil? keys) identity #(select-keys % keys))
             data (->> (get-in data path)
                       (map-vals select-keys*)
                       (str)
                       ((aget js/LZString "compress")))]
         (assoc-in cofx [:effects :local-storage] {key data}))))))
