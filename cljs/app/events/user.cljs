(ns app.events.user
  (:require [re-frame.core :refer [dispatch reg-event-db reg-event-fx
                                   trim-v path debug]]
            [ajax.core :as ajax]
            [app.events.utils :refer [start-fetching stop-fetching cache uncache
                                      send-token save-token]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Fetch auth status
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :fetch-auth-status
  [start-fetching (path :user) (uncache :user) send-token]
  (fn [_ _]
    {:http {:method :get
            :url "/api/protected/users/current"
            :on-success [:success-fetch-auth-status]
            :on-failure [:fail-fetch-auth-status]}}))

(reg-event-fx :success-fetch-auth-status
  [trim-v stop-fetching (path :user) (cache :user)]
  (fn [_ [user]]
    {:db user
     :dispatch [:fetch-orders]}))

(reg-event-db :fail-fetch-auth-status
  [trim-v stop-fetching (path :user)]
  (constantly nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Sign in
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :sign-in
  [trim-v start-fetching send-token]
  (fn [_ [{:keys [email password]}]]
    {:http {:method :post
            :url "/api/auth/sign_in"
            :body {:email email, :password password}
            :on-success [:success-sign-in]
            :on-failure [:fail-sign-in]}}))

(reg-event-fx :success-sign-in
  [trim-v stop-fetching save-token]
  (fn [_ [body headers]]
    {:dispatch [:fetch-auth-status]}))

(reg-event-fx :fail-sign-in
  [trim-v stop-fetching]
  (fn [_ [{:keys [errors]}]]
    {:dispatch-n (map (fn [error] [:notify error])
                      errors)}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Sign out
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :sign-out
  [trim-v start-fetching send-token]
  (fn [_ _]
    {:http {:method :get
            :url "/api/auth/sign_out"
            :on-success [:success-sign-out]}}))

(reg-event-db :success-sign-out
  [trim-v stop-fetching (path :user) (cache :user)]
  (constantly nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Sign up
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :sign-up
  [trim-v start-fetching]
  (fn [_ [{:keys [first-name last-name middle-name email password password-confirmation]}]]
    {:http-xhrio {:method :post
                  :uri "/api/auth"
                  :format (ajax/json-request-format)
                  :response-format (ajax/json-response-format {:keywords? true})
                  :params {:first_name first-name
                           :last_name last-name
                           :middle_name middle-name
                           :email email
                           :password password
                           :password_confirmation password-confirmation
                           :confirm_success_url (.-origin js/location)}
                  :on-success [:success-sign-up]
                  :on-failure [:fail-sign-up]}}))

(reg-event-fx :success-sign-up
  [trim-v stop-fetching]
  (fn [_ [data]]
    (println data)))

(reg-event-fx :fail-sign-up
  [trim-v stop-fetching]
  (fn [_ [data]]
    (let [errors (get-in data [:response :errors :full_messages])]
      (print data)
      {:dispatch-n (map (fn [error] [:notify error])
                        errors)})))
