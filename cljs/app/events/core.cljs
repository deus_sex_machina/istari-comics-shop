(ns app.events.core
  (:require [re-frame.core :refer [reg-event-db reg-event-fx reg-fx
                                   trim-v path]]
            [clojure.browser.event :as gevent]
            [clojure.browser.net :as gnet]
            [clojure.string :refer [join]]
            [accountant.core :refer [navigate!]]
            [day8.re-frame.async-flow-fx]
            [day8.re-frame.http-fx]
            [plumbing.core :refer [fn->>]]
            [app.events.cart]
            [app.events.order]
            [app.events.fetching]
            [app.events.user]
            [app.db :as db :refer [default-db]]
            [app.events.utils :refer [read-response save-token send-token]]))

(reg-event-db :set-page
  [trim-v (path :page)]
  (fn [_ [page]]
    page))

(reg-event-fx :init
  (fn [_ _]
    (print default-db)
    {:db default-db
     :dispatch-n [[:fetch-auth-status]
                  [:fetch-categories]
                  [:fetch-series]
                  [:fetch-products]
                  [:fetch-features]
                  [:fetch-pages]
                  [:fetch-socials]
                  [:uncache-cart]]}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; HTTP fetcher
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def create-body-get
  (fn->> (map (fn [[k v]] (str (name k) "=" v)))
         (join "&")
         (str "?")))

(def create-body-post
  (fn->> (clj->js)
         (.stringify js/JSON)))

(reg-fx :http
  (fn [{:keys [method url body on-success on-failure headers drop-json-on-fail] :as arg}]
    (let [body (if (= method :get)
                 (create-body-get body)
                 (create-body-post body))
          url (if (= method :get) (str url body) url)
          body (when-not (= method :get) body)
          method (name method)
          headers (merge {"Content-Type" "application/json"} headers)]
      (doto (gnet/xhr-connection)
        (gevent/listen "success" #(read-response % on-success false))
        (gevent/listen "error"   #(read-response % on-failure drop-json-on-fail))
        (gnet/transmit url method body headers)))))



(reg-fx :redirect
  (fn [{:keys [url data]}]
    (.redirect js/jQuery url data)))

(reg-event-db :start-fetching
  [(path :fetchers)]
  (fn [fetchers _]
    (inc fetchers)))

(reg-event-db :stop-fetching
  [(path :fetchers)]
  (fn [fetchers _]
    (dec fetchers)))

(reg-fx :save-token
  (fn [[token client uid]]
    (.setItem js/localStorage :access-token token)
    (.setItem js/localStorage :client client)
    (.setItem js/localStorage :uid uid)))

(reg-fx :local-storage
  (fn [data]
    (doseq [[name value] data]
      (.setItem js/localStorage name value))))

(reg-fx :navigate navigate!)

(reg-event-db :notify
  [trim-v]
  (fn [db [text]]
    (let [new-id (:notifications-next-id db)
          new-notification {:text text
                            :level :info
                            :id new-id}]
      (-> db
          (update :notifications-next-id inc)
          (update :notifications assoc new-id new-notification)))))

(reg-event-db :hide-notification
  [trim-v (path :notifications)]
  (fn [notifications [id]]
    (dissoc notifications id)))
