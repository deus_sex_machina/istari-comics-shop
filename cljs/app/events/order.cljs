(ns app.events.order
  (:require [re-frame.core :refer [dispatch reg-event-db reg-event-fx
                                   trim-v path debug]]
            [app.events.utils :refer [start-fetching stop-fetching send-token]]
            [plumbing.core :refer [letk]]
            [clojure.set :refer [rename-keys]]
            [app.db :as db :refer [collection->clj order->clj]]
            [app.routes :refer [order-process-path]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Create order
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :create-order
  [send-token start-fetching (path :cart)]
  (fn [{cart :db} _]
    (let [cart-items (map (fn [[product-id count]]
                            {:product_id product-id
                             :count count})
                          cart)]
      {:http {:method :post
              :url "/api/protected/users/current/orders"
              :body {:cart_items cart-items}
              :on-success [:create-order-success]
              :on-failure [:notify]}})))

(reg-event-fx :create-order-success
  [trim-v stop-fetching]
  (fn [_ [order-id]]
    {:dispatch-n [[:clear-cart]
                  [:fetch-orders]]
     :navigate (order-process-path {:id order-id})}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Fetch postcalc price
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :fetch-postcalc-price
  [trim-v send-token (path :orders)]
  (fn [{orders :db} [order-id]]
    (let [index (get-in orders [order-id :index])
          weight (get-in orders [order-id :weight])]
      {:http {:method :get
              :url "/api/protected/postcalc"
              :body {"postcalc[index]" index
                     "postcalc[weight]" weight}
              :on-success [:success-fetch-postcalc-price order-id]
              :on-failure [:failure-fetch-postcalc-price order-id]
              :drop-json-on-fail true}})))

(reg-event-db :success-fetch-postcalc-price
  [trim-v (path :orders)]
  (fn [orders [order-id {price :price}]]
    (assoc-in orders [order-id :delivery-price] price)))

(reg-event-db :failure-fetch-postcalc-price
  [trim-v (path :orders)]
  (fn [orders [order-id]]
    (assoc-in orders [order-id :delivery-price] nil)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Update order locally
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-db :set-order-property
  [trim-v (path :orders)]
  (fn [orders [order-id prop value]]
    (assoc-in orders [order-id prop] value)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Process order
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :process-order
  [trim-v]
  (fn [{db :db} [order-id]]
    (let [user-id (get-in db [:user :id])
          url (str "/api/protected/users/" user-id "/orders/" order-id "/delivery/post")
          data (-> db
                   (get-in [:orders order-id])
                   (->> (merge {:first-name ""
                                :middle-name ""
                                :last-name ""
                                :comment ""
                                :address ""
                                :index ""
                                :country ""}))
                   (select-keys [:first-name :middle-name :last-name
                                 :comment :address :index :country])
                   (rename-keys {:first-name :first_name
                                 :middle-name :middle_name
                                 :last-name :last_name})
                   (clj->js))]
      {:redirect {:url url
                  :data data}})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Merge order
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(reg-event-fx :merge-orders
  [trim-v send-token]
  (fn [{db :db} [order-id old-order-id]]
    (let [user-id (get-in db [:user :id])]
      {:http {:method :put
              :url (str "/api/protected"
                        "/users/" user-id
                        "/orders/" order-id
                        "/merge/" old-order-id)
              :on-success [:fetch-orders]
              :on-failure [:notify]
              :drop-json-on-fail true}})))
