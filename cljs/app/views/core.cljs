(ns app.views.core
  (:require [reagent.core  :as reagent :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk]]

            [app.views.pages.index :refer [index-page]]
            [app.views.pages.product :refer [product-page]]
            [app.views.pages.category :refer [category-page]]
            [app.views.pages.all-series :refer [all-series-page]]
            [app.views.pages.series :refer [series-page]]
            [app.views.pages.cart :refer [cart-page]]
            [app.views.pages.orders :refer [orders-page]]
            [app.views.pages.order-proccessing :refer [order-proccessing-page]]
            [app.views.pages.order-payment :refer [order-payment-page]]
            [app.views.pages.sign-in :refer [sign-in-page]]
            [app.views.pages.sign-up :refer [sign-up-page]]

            [app.views.notifications :refer [notifications]]
            [app.views.cart :refer [cart]]
            [app.routes :refer [index-path category-path all-series-path
                                orders-path cart-path page-path series-path
                                sign-in-path sign-up-path]]))

(defn spinner []
  (with-let [fetching? (subscribe [:fetching?])]
    [:div
      (when @fetching?
        [:div {:style {:position :fixed
                       :bottom 0
                       :left 0
                       :right 0
                       :z-index 99999
                       :background "rgba(0, 0, 0, .9)"
                       :font-weight :bold
                       :font-size "1.5em"
                       :padding "20px 40px"
                       :color :white}}
         "Loading..."])]))

(def collapsed? (reagent/atom true))

(defn header-brand []
  [:a.navbar-brand.mr-2.float-md-down-inherit {:href (index-path)}
   [:img.pr-1 {:src "/logo.png"}]
   "Истари Комикс"])

(defn header-cart-badge [button?]
  (with-let [cart? (subscribe [:cart?])
             cart-size (subscribe [:cart-size])]
    (when @cart?
      [:div.tag.tag-pill.tag-danger
       {:hidden (and button? (not @collapsed?))
        :class (when button? :tag-badge--button)
        :style {:margin-left (when-not button? 5)}}
       @cart-size])))

(defn header-toggler []
  [:button.navbar-toggler.float-xs-right.hidden-lg-up
   {:type :button
    :on-click #(swap! collapsed? not)}
   [:i.fa.fa-lg {:class (if @collapsed? :fa-bars :fa-times)}]
   [header-cart-badge true]])

(defn header-item [text page link right?]
  (with-let [active-page (subscribe [:page])]
    [:a.nav-item.nav-link
     {:class (str (when (= page @active-page) "active")
                  " "
                  (when right? "float-xs-right"))
      :href link}
     text]))

(defn header-items-left []
  (with-let [categories (subscribe [:categories])]
    [:ul.nav.navbar-nav
     (for [{:keys [id name products]} @categories]
       (when (seq products)
         ^{:key id}
         [header-item name [:category id] (category-path {:id id})]))
     [header-item "Серии" [:all-series] (all-series-path)]]))

(defn header-item-user []
  (with-let [user (subscribe [:user])
             signed-in? (subscribe [:signed-in?])]
    (let [first-name (:first_name @user)]
      (when @signed-in?
        [:span.navbar-text.float-lg-right.mr-1 first-name]))))

(defn header-items-right []
  (with-let [signed-in? (subscribe [:signed-in?])]
    [:div
     [:div.nav.navbar-nav.float-lg-right
      (when @signed-in?
        [header-item "Заказы" [:orders] (orders-path)])
      (when-not @signed-in?
        [header-item "Вход" [:sign-in] (sign-in-path)])
      (when-not @signed-in?
        [header-item "Регистрация" [:sign-up] (sign-up-path)])
      [header-item
       [:span "Корзина" [header-cart-badge false]] [:cart] (cart-path)]]
     [header-item-user]]))

(defn header-items []
  [:div.navbar-toggleable-md.collapse
   {:class (when-not @collapsed? :in)
    :on-click #(reset! collapsed? true)}
   ; [header-separator]
   ; [header-separator]
   [header-items-left]
   [header-items-right]])

(defn header []
  [:nav.navbar.navbar-light.navbar-istari.navbar-fixed-top.navbar-istari--no-border
   [:div.container
    [header-brand]
    [header-toggler]
    [header-items]]])

(defn footer-copyrights []
  [:div.col-xs-12.col-lg-3.text-lg-right
   [:div.page-footer__copyrights
    [:div (char 169) " 2016 Истари Комикс"]]])

(defn footer-pages []
  (with-let [pages (subscribe [:pages])]
    [:div.col-xs-12.col-sm-4.col-lg-2
     [:div.page-footer__group-header "Истари комикс"]
     [:ul.page-footer__group-list
      (for [{:keys [id title url]} @pages]
        ^{:key id}
        [:li
         [:a {:href (page-path {:url url})}
          title]])]]))

(defn footer-series []
  (with-let [series (subscribe [:series])]
    [:div.col-xs-12.col-sm-4.col-lg-2
     [:div.page-footer__group-header "Серии"]
     [:ul.page-footer__group-list
      (for [{:keys [id name]} @series]
        ^{:key id}
        [:li
         [:a {:href (series-path {:id id})}
          name]])]]))

(defn footer-categories []
  (with-let [categories (subscribe [:categories])]
    [:div
     [:div.page-footer__group-header "Категории"]
     [:ul.page-footer__group-list
      (for [{:keys [id name]} @categories]
        ^{:key id}
        [:li
         [:a {:href (category-path {:id id})}
          name]])]]))

(defn footer-profile []
  (with-let [signed-in? (subscribe [:signed-in?])]
    [:div
     [:div.page-footer__group-header "Личный кабинет"]
     [:ul.page-footer__group-list
       (if @signed-in?
         [:li>a {:href "#", :on-click #(dispatch [:sign-out])} "Выйти"]
         (list ^{:key 1} [:li [:a {:href (sign-in-path)} "Войти"]]
               ^{:key 2} [:li [:a {:href (sign-up-path)} "Зарегистрироваться"]]))]]))

(defn footer-profile-and-categories []
  [:div.col-xs-12.col-sm-4.col-lg-2
   [footer-profile]
   [footer-categories]])

(defn footer-socials []
  (with-let [socials (subscribe [:socials])]
    [:div.col-xs-12.col-lg-3.m-b-2
     [:ul.page-footer__social-list
      (for [{:keys [id icon name link]} @socials]
        ^{:key id}
        [:li
         [:a {:href link
              :title name
              :target :_blank}
          [:i.fa.fa-2x {:class (str "fa-" icon)}]]])]]))

(defn footer []
  (with-let [pages (subscribe [:pages])]
    [:footer.page-footer
     [:div.container
      [:div.row.text-xs-center.text-lg-left
       [footer-socials]
       [footer-profile-and-categories]
       [footer-series]
       [footer-pages]
       [footer-copyrights]]]]))

(defn text-page []
  (with-let [page (subscribe [:active-page])]
    [:div.container
     [:div.row>div.col-xs-12.mt-3.mb-1
      [:h1 (:title @page)]]
     [:div.row>div.col-xs-12.mb-3.mt-1
      {:dangerouslySetInnerHTML
       {:__html (:content @page)}}]]))

(defn active-page []
  (with-let [page (subscribe [:page])
             signed-in? (subscribe [:signed-in?])]
    (let [[page id] @page]
      (cond
        (= page :index)      [index-page]
        (= page :all-series) [all-series-page]
        (= page :sign-in)    [sign-in-page]
        (= page :sign-up)    [sign-up-page]
        (= page :cart)       [cart-page]
        (= page :orders)     [orders-page]
        (= page :product)    [product-page id]
        (= page :category)   [category-page id]
        (= page :series)     [series-page id]
        (= page :page)       [text-page]
        (and @signed-in? (= page :order-proccessing)) [order-proccessing-page id]
        (and @signed-in? (= page :order-payment))     [order-payment-page id]
        :else [:div]))))

(defn app []
  [:div
   [spinner]
   [header]
   [active-page]
   [notifications]
   [footer]])
