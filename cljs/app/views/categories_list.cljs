(ns app.views.categories-list
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk]]
            [app.views.products-list :refer [products-list]]
            [app.routes :refer [category-path]]))

(defnk categories-list-item [id name products]
  (when (seq products)
    [:div.container.my-2>div.row>div.col-xs-12
     [:a.h2.category-label.mb-2 {:href (category-path {:id id})} name]
     [products-list products]]))

(defn categories-list [categories]
  [:div
   (for [{:keys [id] :as category} categories]
     ^{:key id}
     [categories-list-item category])])
