(ns app.views.series-list
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk]]
            [app.views.products-list :refer [products-list]]
            [app.routes :refer [series-path]]))

(defnk series-list-item [id name products]
  (when (seq products)
    [:div.container.my-2>div.row>div.col-xs-12
     [:a.h2.category-label.mb-2 {:href (series-path {:id id})} name]
     [products-list products]]))

(defn series-list [series]
  [:div
   (for [{:keys [id] :as one-series} series]
     ^{:key id}
     [series-list-item one-series])])
