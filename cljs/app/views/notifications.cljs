(ns app.views.notifications
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk]]))

(defnk notification [id text level]
  [:div.alert.alert-info.alert-dismissible
   [:button.close {:on-click #(dispatch [:hide-notification id])}
    [:span (char 215)]]
   [:span text]])

(defn notifications []
  (with-let [list (subscribe [:notifications])]
    [:div.notifications
      (for [{:keys [id] :as item} @list]
        ^{:key id}
        [notification item])]))
