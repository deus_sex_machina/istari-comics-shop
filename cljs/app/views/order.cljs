(ns app.views.order
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk letk]]
            [app.views.utils :refer [currency]]
            [app.routes :refer [order-process-path product-path]]))

(defnk order-item [product-id preorder-id count available?]
  (with-let [product (subscribe [:product product-id])]
    (letk [[title] @product
           preorder? (not (nil? preorder-id))]
      [:tr {:class (when-not available? :table-warning)}
       [:td [:a {:href (product-path {:id product-id})} title]]
       [:td (when preorder? "По предзаказу")]
       [:td count]])))

(defnk order [id order-items status weight price]
  [:div
   [:a.h3 {:href (when (= status :created) (order-process-path {:id id}))}
    "Заказ №" id " - " status]
   [:h5.text-muted "Вес: " weight]
   [:h5.text-muted
    "Цена"
    (when (= status :created) " без доставки")
    ": " (currency price)]

   [:table.table.table-striped>tbody
    (for [{:keys [id] :as item} order-items]
      ^{:key id}
      [order-item item])]])
