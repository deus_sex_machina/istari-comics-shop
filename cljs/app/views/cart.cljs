(ns app.views.cart
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk letk]]
            [app.views.utils :refer [currency]]
            [app.routes :refer [product-path cart-path]]))

(defnk cart-item [product count]
  (letk [[title price id] product]
    [:tr
     [:td [:a {:href (product-path {:id id})} title]]
     [:td price "p"]
     [:td " x " count]
     [:td [:button.btn.btn-danger {:on-click #(dispatch [:delete-from-cart id])} "x"]]]))

(defn cart []
  (with-let [cart (subscribe [:cart])
             cart? (subscribe [:cart?])
             total (subscribe [:cart-total])]
    (if-not @cart?
      [:h1.my-3 "Корзина пуста"]
      [:div
       [:h1.my-3 "Корзина (" (currency @total) ")"]
       [:table.table
        [:tbody
         (doall
           (for [{:keys [product-id] :as data} @cart]
             ^{:key product-id}
             [cart-item data]))]]
       [:div.text-xs-center
        [:a.btn.btn-primary.mt-1
         {:href (cart-path)
          :style {:width 200}}
         "Корзина"]
        [:button.btn.btn-warning.mt-1
         {;; :on-click #(dispatch [:froze-order])
          :style {:width 200}}
         "Перейти к оплате"]]])))
