(ns app.views.products-list
  (:require [reagent.core :as reagent :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [letk fn->]]
            [accountant.core :refer [navigate!]]
            [app.views.utils :refer [currency]]
            [app.routes :refer [product-path category-path series-path]]))

(defn products-list-item [id]
  (with-let [product (subscribe [:product id])
             cover-loaded? (reagent/atom false)]
    (letk [[id title price addable? cover-url cover-thumb
            category category-id series series-id] @product
           category-name (:name category)
           series-name (:name series)
           path (product-path {:id id})]
      [:div.col-xs-6.col-sm-4.col-md-4.col-lg-3.col-xl-2-dot-4.product-card__col
       [:div.product-card__wrapper
        [:a.product-card
         {:href "#"
          :on-click (fn [e]
                      (.preventDefault e)
                      (navigate! path))}
         [:img.product-card__image.product-card__image--thumb {:src cover-thumb}]
         [:img.product-card__image
          {:class (when-not @cover-loaded? :product-card__image--hidden)
           :on-load #(reset! cover-loaded? true)
           :src cover-url}]
         [:div.product-card__panel
          [:div.product-card__panel__name title]]
         [:div.product-card__buttons__wrapper
          [:div.product-card__buttons
           [:div.product-card__buttons__button.product-card__buttons__button--to-cart
            {:on-click (fn [e]
                         (.preventDefault e)
                         (.stopPropagation e)
                         (dispatch [:update-cart-count id 1]))
             :disabled (not addable?)}
            [:div.product-card__buttons__button__content
             [:div [:i.fa.fa-shopping-cart] " + 1"]
             [:div (currency price)]]]
           [:div.product-card__buttons__button.product-card__buttons__button--open
            [:div.product-card__buttons__button__content
             [:i.fa.fa-3x.fa-info]]]]]]]])))

         ; [:div.card
         ;  [:img.card-img-top {:style {:width "100%"}
         ;                      :src cover-url}]
         ;  [:div.card-block
         ;   [:a.d-block.h4.card-title {:href (product-path {:id id})} title]
         ;   [:a.d-block.h6.card-title {:href (category-path {:id category-id})} category-name]
         ;   [:a.d-block.h6.card-title {:href (series-path {:id series-id})} series-name]
         ;   [:button.btn.btn-primary
            ; {:on-click #(dispatch [:update-cart-count id 1])
            ;  :disabled (not addable?)}]]]]]])))
         ;    price "p"]]]]]])))

(defn products-list [products]
  [:div.row
   (for [id products]
     ^{:key id}
     [products-list-item id])])
