(ns app.views.pages.product
  (:require [reagent.core :refer [atom] :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk fn->]]
            [app.views.utils :refer [currency]]
            [app.routes :refer [category-path series-path product-path]]))

(defnk product-header [title]
  [:div.container
   [:h1 title]])

(defnk product-subheader [series-id category-id category series]
  [:div.container.my-1
   [:h6
     [:a {:href (category-path {:id category-id})} (:name category)]
     [:span " / "]
     [:a {:href (series-path {:id series-id})} (:name series)]]])

(defn product-cover [{:keys [cover-url]}]
  [:div.col-md-4.mt-1
   [:img.product__cover.img-fluid {:src cover-url}]])

(defn product-description [{:keys [description]}]
  [:div.product__description.mt-2
   [:p.mb-0 description]])

(defn product-form [{:keys [id available price in-cart addable?]}]
  (with-let [counter (atom 1)
             submit (fn [e]
                      (.preventDefault e)
                      (dispatch [:update-cart-count id @counter])
                      (reset! counter 1))]
    (let [available (-> (- available in-cart) (max 0))
          subtotal (* @counter price)
          update-counter! (fn-> (max 1) (min available) (->> (reset! counter)))]
      [:form.mt-2
       {:on-submit submit}
       [:div.product__counter.mr-2.mb-1
        [:div.input-group
         [:span.input-group-btn
          [:button.btn
           {:type :button
            :on-click #(-> @counter dec update-counter!)
            :disabled (not addable?)}
           "-"]]
         [:input.form-control
          {:type :number
           :disabled (not addable?)
           :value @counter
           :on-change (fn-> .-target .-value int update-counter!)}]
         [:span.input-group-btn
          [:button.btn
           {:type :button
            :on-click #(-> @counter inc update-counter!)
            :disabled (not addable?)}
           "+"]]]]
       [:button.btn.btn-primary.btn--round
        {:disabled (not addable?)
         :type :submit}
        "В корзину (" (currency subtotal) ")"]])))

(defn product-main [{:keys [price] :as product}]
      [:div.container>div.row
       [product-cover product]
       [:div.col-md-7.offset-md-1
        [:h1.product__price.mt-1 (currency price)]
        [product-form product]
        [product-description product]]])

; (defnk product-form [id price available addable? in-cart]
;   (with-let [counter (atom 1)]
;     (let [available (-> (- available in-cart) (max 0))
;           subtotal (* @counter price)]
;       [:form.input-group
;        {:style {:max-width 300}
;         :on-submit (fn [e]
;                      (.preventDefault e)
;                      (dispatch [:update-cart-count id @counter])
;                      (reset! counter 1))}
;        [:input.form-control
;         {:type :number
;          :value @counter
;          :disabled (not addable?)
;          :min 1
;          :max available
;          :on-change (fn-> .-target .-value
;                           (max 1)
;                           (min available)
;                           (int)
;                           (->> (reset! counter)))}]
;        [:span.input-group-btn
;         [:button.btn.btn-primary {:disabled (not addable?)
;                                   :type :submit}
;          "Add to cart (+" (currency subtotal) ")"]]])))

(defnk product-reviews [id]
  (with-let [reviews (subscribe [:reviews-for-product id])]
    (if-not (empty? @reviews)
      [:div.mt-3
       [:h3.mb-2 "Reviews"]
       (for [{:keys [id name text] :as review} @reviews]
         ^{:key id}
         [:blockquote.blockquote
          [:div {:dangerouslySetInnerHTML {:__html text}}]
          [:footer.blockquote-footer name]])])))

(defn product-page [id]
  (with-let [product (subscribe [:active-product])]
    (if-not @product
      [:div.container>h1.my-3 "Загрузка"]
      [:div.product.my-3
       [product-header @product]
       [product-subheader @product]
       [product-main @product]])))
       ; [product-reviews @product]]))
