(ns app.views.pages.orders
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            ; [plumbing.core :refer [defnk letk]]
            [app.views.order :refer [order]]
            [app.routes :refer [product-path
                                category-path
                                order-process-path
                                order-pay-path]]))

(defn order* [{:keys [id status] :as item}]
  (let [created?   (= status :created)
        processed? (= status :processed)]
    [:div.my-3.col-xs-12
     [order item]
     (when created?
       [:a.btn.btn-primary {:href (order-process-path {:id id})}
        "Proccess"])
     (when processed?
       [:a.btn.btn-primary {:href (order-pay-path {:id id})}
        "Pay"])]))

(defn orders-page []
  (with-let [orders (subscribe [:orders])]
    [:div.container.my-3>div.row
     [:h1.col-xs-12 "Заказы"]
     (for [{:keys [id] :as item} @orders]
       ^{:key id}
       [order* item])]))
