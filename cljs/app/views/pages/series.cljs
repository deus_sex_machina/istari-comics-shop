(ns app.views.pages.series
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [app.views.categories-list :refer [categories-list]]))

(defn series-page [id]
  (with-let [series (subscribe [:active-series])
             categories (subscribe [:categories-in-active-series])]
    (let [series-name (:name @series)]
      [:div.container.my-3>div.row
       [:h1.col-xs-12 series-name]
       [:div.col-xs-12.my-1
        {:dangerouslySetInnerHTML
         {:__html (:description @series)}}]
       [categories-list @categories]])))
