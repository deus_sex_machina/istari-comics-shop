(ns app.views.pages.category
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [app.views.series-list :refer [series-list]]))

(defn category-page []
  (with-let [category (subscribe [:active-category])
             series (subscribe [:series-in-active-category])]
    (let [category-name (:name @category)]
      [:div.container.my-3>div.row
       [:h1.col-xs-12 category-name]
       [:div.col-xs-12.my-1
        {:dangerouslySetInnerHTML
         {:__html (:description @category)}}]
       [series-list @series]])))
