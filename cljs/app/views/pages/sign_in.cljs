(ns app.views.pages.sign-in
  (:require [reagent.core :as reagent :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [accountant.core :refer [navigate!]]
            [app.routes :refer [index-path]]
            [app.views.pages.sign-layout :refer [sign-layout form-input form-submit]]))

(defn handle-submit [email password]
  (fn [e]
    (.preventDefault e)
    (dispatch [:sign-in {:email email, :password password}])))

(defn form []
  (with-let [email (reagent/atom "")
             password (reagent/atom "")]
    [:form
     {:on-submit (handle-submit @email @password)}
     [form-input {:icon "envelope"
                  :type :email
                  :placeholder "Email"
                  :name "email"
                  :atom email}]
     [form-input {:icon "lock"
                  :type :password
                  :placeholder "Пароль"
                  :name "password"
                  :atom password}]
     [form-submit "Войти"]]))

(defn sign-in-page []
  (with-let [signed-in? (subscribe [:signed-in?])]
    (when @signed-in?
      (navigate! (index-path)))
    [sign-layout (constantly [:span "Вход"])
                 (constantly [:span "Войдите в свой аккаунт"])
                 form]))
