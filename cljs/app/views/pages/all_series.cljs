(ns app.views.pages.all-series
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [app.views.series-list :refer [series-list]]
            [app.views.cart :refer [cart]]))

(defn all-series-page []
  (with-let [series (subscribe [:series])]
    [:div.container.my-3>div.row
     [:h1.col-xs-12 "Все серии"]
     [series-list @series]]))
