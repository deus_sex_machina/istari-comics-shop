(ns app.views.pages.cart
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk letk]]
            [app.views.utils :refer [currency]]
            [app.routes :refer [product-path category-path]]))

(defnk cart-item [product count]
  (letk [[id title price category-id category available] product
         category-name (:name category)]
    [:tr
     [:td {:style {:vertical-align :middle}}
      [:a {:href (product-path  {:id id})} title]]
     [:td {:style {:vertical-align :middle}}
      [:a {:href (category-path {:id category-id})} category-name]]
     [:td {:style {:vertical-align :middle}}
      (currency price)]
     [:td.text-xs-right
      [:div.form-inline
       [:div.form-group
        [:label.mr-1.font-weight-bold "Количество: "]
        [:input.form-control {:value count
                              :type :number
                              :min 1
                              :max available
                              :style {:width 80}
                              :on-change #(let [count (-> % .-target .-value)]
                                            (dispatch [:set-cart-count id count]))}]]]]
     [:td.text-xs-right
      [:button.btn.btn-danger {:on-click #(dispatch [:delete-from-cart id])} "Убрать"]]]))

(defn cart-page []
  (with-let [cart (subscribe [:cart])
             cart? (subscribe [:cart?])
             total (subscribe [:cart-total])]
    [:div.container>div.row
     (if-not @cart?
       [:h1.my-3.col-xs-12 "Корзина пуста"]
       [:div.col-xs-12
        [:h1.my-3 "Корзина"]
        [:table.table.my-3
         [:tbody
          (doall
            (for [{:keys [product-id] :as data} @cart]
              ^{:key product-id}
              [cart-item data]))]
         [:tfoot
          [:tr>td.pt-2 {:col-span "100%"}
           [:h3.text-xs-right "Итого: " [:b (currency @total)]]]
          [:tr>td.pt-2.text-xs-right {:col-span "100%"}
           [:button.btn.btn-warning.mt-1 {:on-click #(dispatch [:create-order])}
            "Создать заказ"]]]]])]))
