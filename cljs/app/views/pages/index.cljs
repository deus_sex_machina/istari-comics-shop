(ns app.views.pages.index
  (:require [reagent.core :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk]]
            [app.views.categories-list :refer [categories-list]]
            [app.routes :refer [page-path]]))

(defnk feature [title icon description page-id]
  (with-let [page (subscribe [:page-by-id page-id])]
    (let [page-url (:url @page)]
      [:div.col-sm-6
       [:a.feature.media
        {:href (when page-id (page-path {:url page-url}))}
        [:div.media-left.feature__icon
         [:i.media-object.fa.fa-5x {:class (str "fa-" icon)}]]
        [:div.media-body.feature__description
         [:h4.media-heading.feature__title title]
         [:p description]]]])))

(defn features []
  (with-let [features (subscribe [:features])]
    (when (seq @features)
      [:div.features>div.container>div.row
       (for [{:keys [id] :as f} @features]
         ^{:key id}
         [feature f])])))

(defn index-page []
  (with-let [categories (subscribe [:categories])]
    [:div
     [:div.container.my-3>div.row
      [:h1.col-xs-12 "Главная"]
      [categories-list @categories]]
     [features]]))
