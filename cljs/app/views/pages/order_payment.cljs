(ns app.views.pages.order-payment
  (:require [reagent.core :as reagent :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [app.views.order :refer [order]]
            [plumbing.core :refer [fn->>]]
            [app.routes :refer [order-pay-path]]))

(defn hidden-input [name value]
  [:input {:type :hidden
           :name name
           :value value}])

(defn checkbox [name ratom label]
  [:div>label
   [:input {:type :checkbox
            :name name
            :checked @ratom
            :on-change (fn->> .-target .-checked (reset! ratom))}]
   [:span {:style {:margin-left 10}}
    label]])

(defn order-payment-page [id]
  (with-let [order-item (subscribe [:order id])
             user  (subscribe [:user])
             defer-shipping? (reagent/atom false)
             before-finish?  (reagent/atom false)]
    (when @order-item
      (let [order-id (:id @order-item)
            user-id (:id @user)]
        [:div.container.my-3>div.row
          [:div.col-xs-12
           [order @order-item]]
          [:form.col-xs-12
           {:method :get
            :action "//paymaster.ru/Payment/Init"}
           [checkbox :defer_shipping defer-shipping? "Отложить доставку"]
           [checkbox :before_finish  before-finish?  "Отправить раньше"]
           [hidden-input "LMI_MERCHANT_ID"    "ebeceb08-4892-4160-9d9e-0edff09297e5"]
           [hidden-input "LMI_PAYMENT_AMOUNT" (:price @order-item)]
           [hidden-input "LMI_CURRENCY"       "RUB"]
           [hidden-input "LMI_PAYMENT_NO"     order-id]
           [hidden-input "LMI_PAYMENT_DESC"   "Istari Comics Books"]
           [hidden-input "LMI_SIM_MODE"       "2"]
           [hidden-input "LMI_PAYER_EMAIL"    (:email @user)]
           [hidden-input "LMI_FAILURE_URL"    (str (.-origin js/location)
                                                   (order-pay-path {:id order-id}))]
           [:button.btn.btn-primary
            {:type :submit}
            "Оплатить через Paymaster"
            (when @before-finish?
              " (без бонусов)")]]]))))
