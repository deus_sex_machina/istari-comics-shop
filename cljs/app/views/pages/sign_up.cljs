(ns app.views.pages.sign-up
  (:require [reagent.core :as reagent :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [accountant.core :refer [navigate!]]
            [app.routes :refer [index-path]]
            [app.views.pages.sign-layout :refer [sign-layout form-input form-submit]]))

(defn handle-submit [first-name last-name middle-name
                     email password password-confirmation]
  (fn [e]
    (.preventDefault e)
    (dispatch [:sign-up {:first-name first-name, :last-name last-name,
                         :middle-name middle-name,
                         :email email, :password password,
                         :password-confirmation password-confirmation}])))

(defn form []
  (with-let [first-name (reagent/atom "")
             last-name (reagent/atom "")
             middle-name (reagent/atom "")
             email (reagent/atom "")
             password (reagent/atom "")
             password-confirmation (reagent/atom "")]
    [:form
     {:on-submit (handle-submit @first-name @last-name @middle-name
                                @email @password @password-confirmation)}
     [form-input {:icon "user"
                  :placeholder "Имя"
                  :name "first_name"
                  :atom first-name}]
     [form-input {:icon "user"
                  :placeholder "Фамилия"
                  :name "last_name"
                  :atom last-name}]
     [form-input {:icon "user"
                  :placeholder "Отчество"
                  :name "middle_name"
                  :atom middle-name}]
     [form-input {:icon "envelope"
                  :type :email
                  :placeholder "Email"
                  :name "email"
                  :atom email}]
     [form-input {:icon "lock"
                  :type :password
                  :placeholder "Пароль"
                  :name "password"
                  :atom password}]
     [form-input {:icon "lock"
                  :type :password
                  :placeholder "Пароль еще раз"
                  :name "password_confirmation"
                  :atom password-confirmation}]
     [form-submit "Зарегистрироваться"]]))

(defn sign-up-page []
  (with-let [signed-in? (subscribe [:signed-in?])]
    (when @signed-in?
      (navigate! (index-path)))
    [sign-layout (constantly [:span "Регистрация"])
                 (constantly [:span "Создайте новый аккаунт"])
                 form]))
