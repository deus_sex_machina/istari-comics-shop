(ns app.views.pages.order-proccessing
  (:require [reagent.core :as reagent :refer-macros [with-let]]
            [re-frame.core :refer [subscribe dispatch]]
            [plumbing.core :refer [defnk letk fn-> indexed]]
            [app.views.utils :refer [currency]]
            [app.views.order :refer [order]]))

(defn order-input [item config]
  (let [callback  (:callback config)
        id        (:id item)
        prop      (:prop config)
        name      (get config :name prop)
        label     (:label config)
        textarea? (:textarea? config)
        value     (get item prop)]
    [:div.form-group.row
     [:label.col-md-6
      [:span label]
      [(if textarea? :textarea :input)
       {:class :form-control
        :type :text
        :value value
        :name name
        :on-change (fn [el]
                     (let [new-value (-> el .-target .-value)]
                       (dispatch [:set-order-property id prop new-value])
                       (when callback (callback))))}]]]))

(defn delivery-price [item]
  (let [price (:price item)
        delivery-price (:delivery-price item)]
    [:label.col-xs-6.px-1
     (currency price)
     (when delivery-price
       [:span " + " (currency delivery-price)])]))

(defn address-input [item]
  (with-let [config #js{:spinner false
                        :token "5887514e0a69de2c418b458d"
                        :oneString true}
             mount-kladr (fn [el]
                           (let [$el (js/jQuery el)
                                 kladr (aget $el "kladr")]
                             (.call kladr $el config)))]
    [:div.form-group.row
     [:label.col-md-6
      [:span "Адрес"]
      [:input.form-control
       {:type :text
        :name "address"
        :ref mount-kladr}]]]))

(defn form-post [item]
  (with-let [user (subscribe [:user])
             access-data (subscribe [:access-data])
             on-form-key-press (fn [event]
                                 (let [key (.-key event)
                                       enter? (= key "Enter")]
                                   (when enter?
                                     (.preventDefault event))))]
    (let [order-id     (:id item)
          user-id      (:id @user)
          first-name   (:first-name item)
          last-name    (:last-name item)
          middle-name  (:middle-name item)
          access-token (:access-token @access-data)
          client       (:client @access-data)
          uid          (:uid @access-data)
          action (str "/api/protected/users/" user-id
                      "/orders/" order-id
                      "/delivery/post")]
      [:form
       {:method :post
        :on-key-press on-form-key-press
        :action action}
       [:input {:type :hidden
                :name :access-token
                :value (str access-token)}]
       [:input {:type :hidden
                :name :client
                :value (str client)}]
       [:input {:type :hidden
                :name :uid
                :value (str uid)}]
       [order-input item {:prop :first-name
                          :label "Имя"
                          :name "first_name"}]
       [order-input item {:prop :last-name
                          :label "Фамилия"
                          :name "last_name"}]
       [order-input item {:prop :middle-name
                          :label "Отчество"
                          :name "middle_name"}]
       [address-input item]
       [order-input item {:prop :index
                          :label "Индекс"
                          :callback #(dispatch [:fetch-postcalc-price order-id])}]
       [delivery-price item]
       [order-input item {:prop :comment
                          :label "Комментарий"
                          :textarea? true}]
       [:button.btn {:type :submit} "Оформить заказ через Почту России"]])))

(defn form-checkout [item]
  (with-let [user (subscribe [:user])
             products* (subscribe [:products*])
             checkout-token (subscribe [:checkout-token])]
    (if-not @checkout-token
      (dispatch [:fetch-checkout-ticket])
      (let [{:keys [id order-items]} item
            {:keys [first-name last-name middle-name]} @user
            full-name (str last-name " " first-name " " middle-name)
            _ (js/setTimeout
                #((aget js/window "openCheckout"))
                100)]
        [:form#checkout
         {:action "//platform.checkout.ru/shop/checkout2"
          :target "copIframe"
          :method :post}
         [:input.btn.btn-primary
          {:type :submit
           :value "Оформить заказ через Checkout"}]
         [:input {:type :hidden
                  :name "callbackURL"
                  :value (str "//istari-shop.herokuapp.com/api/callbacks"
                              "/orders/" id
                              "/delivery/checkout/success")}]
         [:input {:type :hidden
                  :name "ver"
                  :value "1"}]
         [:input {:type :hidden
                  :name "ticket"
                  :value @checkout-token}]
         [:input {:type :hidden
                  :name "fullname"
                  :value full-name}]
         (doall
           (for [[index {:keys [id product-id
                                count price weight] :as order-item}]
                 (indexed order-items)]
             (let [product (get @products* product-id)]
               ^{:key id}
               [:span
                [:input {:type :hidden
                         :name (str "names[" index "]")
                         :value (:title product)}]
                [:input {:type :hidden
                         :name (str "codes[" index "]")
                         :value (:article product)}]
                [:input {:type :hidden
                         :name (str "quantities[" index "]")
                         :value count}]
                [:input {:type :hidden
                         :name (str "costs[" index "]")
                         :value price}]
                [:input {:type :hidden
                         :name (str "paycosts[" index "]")
                         :value price}]
                [:input {:type :hidden
                         :name (str "weights[" index "]")
                         :value weight}]])))]))
    (finally
      (dispatch [:clear-checkout-token]))))

(defn form-merge [{:keys [id]}]
  (with-let [orders (subscribe [:orders-for-merge id])
             new-id id]
    [:table.table.table-striped>tbody
     (for [{:keys [id]} @orders]
       ^{:key id}
       [:tr
        [:td {:style {:vertical-align :middle}}
         [:h5.m-0 "Заказ #" id]]
        [:td.text-xs-right
         [:button.btn.btn-primary
          {:on-click #(dispatch [:merge-orders new-id id])}
          "Объединить"]]])]))

(defn tab-item [label container component]
  [:li.nav-item
   [:a.nav-link
    {:class (when (= @container component) :active)
     :on-click #(reset! container component)
     :href ""}
    label]])

(defn forms [item]
  (with-let [current-form (reagent/atom :post)]
    [:div.col-xs-12
     [:ul.nav.nav-tabs
      [tab-item "Почта России" current-form :post]
      [tab-item "Checkout" current-form :checkout]
      [tab-item "Объединение заказов" current-form :merge]]
     [:div.tab-content.mt-1
      [:div.tab-pane.fade.in.active
       (case @current-form
         :post     [form-post @item]
         :checkout [form-checkout @item]
         :merge    [form-merge @item])]]]))

(defn order-proccessing-page [id]
  (with-let [item (subscribe [:order id])]
    (when @item
      [:div.container.my-3>div.row
        [:div.col-xs-12
         [order @item]]
        [forms item]])))
