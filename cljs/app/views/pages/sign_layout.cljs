(ns app.views.pages.sign-layout
  (:require [plumbing.core :refer [fn->>]]))

(defn sign-layout [header subheader body]
  [:div.devise>div.container>div.row
   [:div.col-lg-8.offset-lg-2.pa-0
    [:div.page-header__wrapper
     [:div.page-header [header]]]
    [:div.box.mb-3
     [:div.box__header [subheader]]
     [body]]]])

(defn form-input [{:keys [icon placeholder type atom name]}]
  [:div.form-group.mb-2
   [:div.form-control__icon__wrapper
    [:input.form-control.form-control--round {:type (or type :text)
                                              :placeholder (or placeholder "")
                                              :value @atom
                                              :name name
                                              :id name
                                              :on-change (fn->> .-target .-value (reset! atom))}]
    [:label.form-control__icon
     {:for name}
     [:i.fa {:class (str "fa-" icon)}]]]])

(defn form-submit [label]
  [:div.text-xs-right
   [:input.btn.btn-primary.btn--round
    {:type :submit
     :value label}]])
