(ns app.subs
  (:require [re-frame.core :refer [reg-sub subscribe]]
            [plumbing.core :refer [?> ?>> fnk letk]]))

(reg-sub :fetching?
  (fn [db _]
    (-> db :fetchers zero? not)))

(reg-sub :page
  (fn [db _]
    (:page db)))

(reg-sub :user
  (fn [db _]
    (:user db)))

(reg-sub :access-data
  (fn [db _]
    (:access-data db)))

(reg-sub :signed-in?
  :<- [:user]
  (fn [user _]
    (not (empty? user))))

(reg-sub :checkout-token
  (fn [db _]
    (:checkout-token db)))

;;;; Notifications

(reg-sub :notifications*
  (fn [db _]
    (:notifications db)))

(reg-sub :notifications
  :<- [:notifications*]
  (fn [notifications* _]
    (->> (vals notifications*)
         (sort-by :id)
         (reverse))))


;;;; Products

(reg-sub :products*
  (fn [db _]
    (:products db)))

(reg-sub :products
  :<- [:products*]
  (fn [products* _]
    (->> (vals products*)
         (sort-by :id))))

(defn join [entity field source]
  (let [field-id (-> field name (str "-id") keyword)
        obj-id (get entity field-id)
        obj (get source obj-id)]
    (assoc entity field obj)))

(defn join-cart [product cart*]
  (letk [[id available] product
         in-cart (get cart* id)]
    (-> product
        (assoc :addable? (> available in-cart))
        (assoc :in-cart in-cart))))

(reg-sub :product
  :<- [:products*]
  :<- [:categories*]
  :<- [:series*]
  :<- [:cart*]
  (fn [[products* categories* series* cart*] [_ id]]
    (some-> (get products* id)
            (join :category categories*)
            (join :series series*)
            (join-cart cart*))))

(reg-sub :active-product
  :<- [:page]
  (fn [[_ product-id] _]
    @(subscribe [:product product-id])))


;;;; Categories

(reg-sub :categories*
  (fn [db _]
    (:categories db)))

(reg-sub :categories**
  :<- [:categories*]
  (fn [categories* _]
    (vals categories*)))

(reg-sub :category
  :<- [:categories*]
  (fn [categories* [_ id]]
    (get categories* id)))

(reg-sub :active-category
  :<- [:page]
  (fn [[_ category-id]]
    @(subscribe [:category category-id])))

;; TODO
(reg-sub :categories
  :<- [:categories**]
  :<- [:products]
  (fn [[categories** products] [_ {:keys [in-series]}]]
    (map
     (fn [{:keys [id] :as category}]
       (assoc category :products
         (->> products
              (filter #(= id (:category-id %)))
              (?>> in-series (filter #(= in-series (:series-id %))))
              (map :id))))
     categories**)))

(reg-sub :categories-in-active-series
  :<- [:page]
  (fn [[_ category-id] _]
    @(subscribe [:categories {:in-series category-id}])))


;;;; Series

(reg-sub :series*
  (fn [db _]
    (:series db)))

(reg-sub :series**
  :<- [:series*]
  (fn [series* _]
    (vals series*)))

(reg-sub :one-series
  :<- [:series*]
  (fn [series* [_ id]]
    (get series* id)))

(reg-sub :active-series
  :<- [:page]
  (fn [[_ series-id] _]
    @(subscribe [:one-series series-id])))

;; TODO
(reg-sub :series
  :<- [:series**]
  :<- [:products]
  (fn [[series** products] [_ {:keys [in-category]}]]
    (map
     (fn [{:keys [id] :as series}]
       (assoc series :products
         (->> products
              (filter #(= id (:series-id %)))
              (?>> in-category (filter #(= in-category (:category-id %))))
              (map :id))))
     series**)))

(reg-sub :series-in-active-category
  :<- [:page]
  (fn [[_ category-id] _]
    @(subscribe [:series {:in-category category-id}])))


;;;; Features

(reg-sub :features*
  (fn [db _]
    (:features db)))

(reg-sub :features
  :<- [:features*]
  (fn [features* _]
    (->> (vals features*)
         (sort-by :id))))


;;;; Pages

(reg-sub :pages*
  (fn [db _]
    (:pages db)))

(reg-sub :pages
  :<- [:pages*]
  (fn [pages* _]
    (->> (vals pages*)
         (sort-by :id))))

(reg-sub :page-by-id
  :<- [:pages*]
  (fn [pages* [_ id]]
    (get pages* id)))

(reg-sub :active-page
  :<- [:pages*]
  :<- [:page]
  (fn [[pages* [_ url]] [_ id]]
    (get pages* url)))


;;;; Socials

(reg-sub :socials*
  (fn [db _]
    (:socials db)))

(reg-sub :socials
  :<- [:socials*]
  (fn [socials* _]
    (->> (vals socials*)
         (sort-by :id))))

;;;; Reviews

(reg-sub :reviews*
  (fn [db _]
    (:reviews db)))

(reg-sub :reviews
  :<- [:reviews*]
  (fn [reviews* _]
    (vals reviews*)))

(reg-sub :reviews-for-product
  :<- [:reviews]
  (fn [reviews [_ product-id]]
    (->> reviews
         (filter #(= (:product-id %) product-id))
         (sort-by :id)
         (reverse))))


;;;; Orders

(reg-sub :orders*
  (fn [db _]
    (:orders db)))

(reg-sub :orders
  :<- [:orders*]
  (fn [orders* _]
    (->> (vals orders*)
         (sort-by :id)
         (reverse))))

(reg-sub :order
  :<- [:orders*]
  (fn [orders* [_ id]]
    (get orders* id)))

(reg-sub :orders-for-merge
  :<- [:orders]
  (fn [orders [_ order-id]]
    (let [available-statuses #{:created :processed :paid :stocked}
          available? #(contains? available-statuses (:status %))]
      (->> orders
           (filter available?)
           (filter #(not= (:id %) order-id))))))

;;;; Cart

(reg-sub :cart*
  (fn [db _]
    (:cart db)))

(reg-sub :cart?
  :<- [:cart*]
  (fn [cart* _]
    (seq cart*)))

(reg-sub :cart
  :<- [:cart*]
  (fn [cart* _]
    (map (fn [[product-id count]]
           {:product-id product-id
            :product @(subscribe [:product product-id])
            :count count})
         cart*)))

(reg-sub :cart-size
  :<- [:cart*]
  (fn [cart* _]
    (->> (vals cart*)
         (reduce +))))

(reg-sub :cart-total
  :<- [:cart]
  (fn [cart _]
    (reduce (fn [total {:keys [product count]}]
              (let [price (:price product)]
                (+ total (* price count))))
            0 cart)))
