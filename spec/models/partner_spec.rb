require "rails_helper"

describe Partner do
  it { is_expected.to validate_presence_of :logo }
end
