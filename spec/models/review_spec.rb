require "rails_helper"

describe Review do
  it { is_expected.to validate_presence_of :text }
end
