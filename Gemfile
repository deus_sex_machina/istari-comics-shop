source "https://rubygems.org"

ruby "2.3.1"

gem "rails", "4.2.7.1"
gem "pg"
gem "jbuilder"

# assets
gem "font-awesome-sass"
gem "jquery-countdown-rails", "~> 2.0", ">= 2.0.2"
gem "jquery-rails"
gem "lodash-rails"
gem "readmorejs-rails"
gem "sass-rails", "~> 5.0.0"
gem "uglifier", ">= 2.7.2"

# views
gem "bootstrap", "~> 4.0.0.alpha5"
gem "slim", ">= 3.0.7"
gem "tether-rails", "~> 1.3", ">= 1.3.3"

# all other gems
gem "carrierwave"
gem "carrierwave-processing"
gem "ckeditor"
gem "decent_decoration"
gem "decent_exposure"
gem "devise", "~> 4.2"
gem "devise_token_auth"
gem "draper"
gem "http"
gem "interactor"
gem "faker"
gem "fog"
gem "kaminari"
gem "mini_magick"
gem "puma"
gem "rack-canonical-host"
gem "rack-mini-profiler", require: false
gem "rails_admin_grid"
gem "rails_admin_simple_has_many"
gem "rails_admin"
gem "recaptcha", require: "recaptcha/rails"
gem "responders"
gem "rollbar"
gem "seedbank"

group :staging, :production do
  gem "newrelic_rpm"
end

group :test do
  gem "capybara"
  gem "capybara-webkit"
  gem "database_cleaner"
  gem "email_spec"
  gem "formulaic"
  gem "launchy"
  gem "shoulda-matchers", require: false
  gem "webmock", require: false
end

group :development, :test do
  gem "brakeman", require: false
  gem "bundler-audit", require: false
  gem "byebug"
  gem "coffeelint"
  gem "dotenv-rails"
  gem "factory_girl_rails"
  gem "meta_request"
  gem "pry-rails"
  gem "rails_best_practices", require: false
  gem "rspec-rails", "~> 3.4"
  gem "rubocop", require: false
  gem "rubocop-rspec", require: false
  gem "scss_lint", require: false
end

group :development do
  gem "bullet"
  gem "foreman", require: false
  gem "letter_opener"
  gem "quiet_assets"
  gem "rails-erd"
  gem "spring"
  gem "spring-commands-rspec"
  gem "web-console", "~> 2.0"
end
