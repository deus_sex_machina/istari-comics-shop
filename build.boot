(set-env! :source-paths #{"cljs"}
          :dependencies '[;; dev dependencies
                          [adzerk/boot-cljs        "1.7.228-1"]
                          [adzerk/boot-reload      "0.4.12"]
                        ;  [pandeiro/boot-http      "0.7.3"]
                        ;  [tolitius/boot-check     "0.1.3"]
                        ;  [boot-deps               "0.1.6"]
                        ;  [proto-repl              "0.3.1"]
                        ;  [adzerk/boot-cljs-repl   "0.3.3"]

                       ;   [com.cemerick/piggieback "0.2.1"  :scope "test"]
                       ;   [weasel                  "0.7.0"  :scope "test"]
                       ;   [org.clojure/tools.nrepl "0.2.12" :scope "test"]

                          ;; project dependencies
                          [org.clojure/clojure         "1.8.0"]
                          [org.clojure/clojurescript   "1.9.229"]
                          [reagent                     "0.6.0"]
                          [re-frame                    "0.8.0"]
                          [cljs-ajax                   "0.5.8"]
                          [day8.re-frame/async-flow-fx "0.0.6"]
                          [day8.re-frame/http-fx       "0.1.2"]
                          [prismatic/plumbing          "0.5.3"]
                          [secretary                   "1.2.3"]
                          [venantius/accountant        "0.1.7"]])

(require '[adzerk.boot-cljs :refer [cljs]]
         '[adzerk.boot-reload :refer [reload]])
         ;'[pandeiro.boot-http :refer [serve]]
         ;'[tolitius.boot-check :as check-source]
         ;'[boot-deps :refer [ancient]]
         ;'[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]])

(task-options!
  target {:dir #{"public/cljs"}})

(defn with-notify [title & handlers]
  (comp
    (notify :title title, :audible true, :visual true)
    (apply comp handlers)))

; (deftask check []
;   (with-notify "Check"
;     (ancient)
;     (check-source/with-yagni)
;     (check-source/with-eastwood)
;     (check-source/with-kibit)
;     (check-source/with-bikeshed)))

(deftask build []
  (comp
    (cljs :compiler-options {:asset-path "/cljs/main.out"}
          :optimizations :advanced)
    (target)))

(deftask dev []
  (comp
    ; (serve :dir "target/")
    (watch)
    (with-notify "Dev build"
      (reload :on-jsload 'app.core/run
              :cljs-asset-path "/cljs/")
      (cljs :compiler-options {:asset-path "/cljs/main.out"})
      (target))))
