def autorescue
  loop do
    begin
      yield
    rescue Exception => e
    end
  end
end

def every(duration)
  loop do
    yield
    sleep duration
  end
end

Thread.new do
  autorescue do
    every 1.hour do
      Order.stocked.each(&:save!)
    end
  end
end
