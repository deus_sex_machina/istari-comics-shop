CarrierWave.configure do |config|
  config.fog_credentials = {
    provider: "AWS",
    aws_access_key_id: ENV["AWS_ACCESS"],
    aws_secret_access_key: ENV["AWS_SECRET"],
    region: "eu-central-1"
  }
  config.fog_directory  = "istari-shop"

  (Rails.env.development? || Rails.env.test?) && config.storage = :file

  Rails.env.production? && config.storage = :fog
end
