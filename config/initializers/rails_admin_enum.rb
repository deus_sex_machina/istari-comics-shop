module ActiveRecord
  module RailsAdminEnum
    # rubocop:disable MethodLength
    # rubocop:disable UnusedBlockArgument
    def enum(definitions)
      super

      definitions.each do |name, values|
        define_method("#{ name }_enum") {
          self.class.send(name.to_s.pluralize).to_a.map do |k, v|
            translated = I18n.t(k, scope: [self.class.to_s.downcase, name.to_s])
            processed_key = translated.include?('translation missing') ? k : translated
            [processed_key, v]
          end
        }

        define_method("#{name}=") do |value|
          if value.is_a?(String) && value.to_i.to_s == value
            super value.to_i
          else
            super value
          end
        end
      end
    end
  end
end

ActiveRecord::Base.send(:extend, ActiveRecord::RailsAdminEnum)
