DATA_LABEL = "Данные"
HOME_LABEL = "Домашняя страница"
CATALOG_LABEL = "Каталог"

RailsAdmin.config do |config|
  config.label_methods.unshift(:display_name)
  config.authenticate_with do
    warden.authenticate! scope: :admin
  end
  config.current_user_method(&:current_admin)

  config.actions do
    dashboard
    index                         # mandatory
    grid do
      except %w( User Category Genre Feature Page Review Author Series Country SocialNetwork Preorder Order PackingCost )
    end
    new do
      except "PackingCost"
    end
    export do
      except %w( Feature Slide Partner SocialNetwork Product Category Series Genre Author Country Page Review PackingCost )
    end
    bulk_delete do
      except "PackingCost"
    end
    show do
      except "PackingCost"
    end
    edit
    delete do
      except "PackingCost"
    end
    show_in_app
  end

  config.main_app_name = "Истари Комикс"

  config.model "User" do
    list do
      field :full_name
      field :email
      field :first_name do
        filterable true
        visible false
      end
      field :last_name do
        filterable true
        visible false
      end
      field :middle_name do
        filterable true
        visible false
      end
    end

    edit do
      field :first_name
      field :last_name
      field :middle_name
      field :email
      field :password
      field :password_confirmation
    end

    show do
      field :email
      field :first_name do
        label "ФИО"
        pretty_value do
          bindings[:object].full_name
        end
      end
      # field :last_name
      # field :middle_name
      field :orders
      field :reviews
      field :confirmed_at
      field :last_sign_in_at
    end

    export do
      field :email
      field :first_name
      field :last_name
      field :middle_name
    end

    navigation_label DATA_LABEL
    weight 0
  end

  config.model "Order" do
    list do
      field :id
      field :user do
        pretty_value do
          value.try(:full_name)
        end
      end
      field :status, :enum do
        pretty_value do
          I18n.t("order.#{name}.#{value}")
        end
      end
      field :price
      field :paid
    end

    edit do
      field :user

      field :status, :enum do
        pretty_value do
          I18n.t("order.#{name}.#{value}")
        end
      end

      field :order_items, :simple_has_many

      field :weight
      field :price
      field :paid

      group :delivery do
        label "Доставка"
        active false

        field :delivery_type, :enum do
          pretty_value do
            I18n.t("order.#{name}.#{value}")
          end
        end

        field :external_id
        field :full_name
        field :country
        field :address
        field :index
        field :cost
        field :defer_shipping
        field :await_preorder
        field :comment
      end
    end

    show do
      field :user do
        pretty_value do
          value.full_name
        end
      end

      field :price do
        label "Заказ"
        pretty_value do
          order = bindings[:object]
          %{
            <table class="table">
              <tbody>
                <tr>
                  <th>Стоимость</th>
                  <td>#{order.price}</td>
                </tr>
                <tr>
                  <th>Оплачено</th>
                  <td>#{order.paid}</td>
                </tr>
                <tr>
                  <th>Вес</th>
                  <td>#{order.weight} г</td>
                </tr>
              </tbody>
            </table>
            <p>#{"Отложена доставка" if order.defer_shipping?}</p>
            <p>#{"Ожидание конца предзаказа" if order.await_preorder?}</p>
          }.html_safe
        end
      end

      field :order_items do
        pretty_value do
          order = bindings[:object]
          %{
            <table class="table">
              <thead>
                <tr>
                  <th> Название </th>
                  <th> Категория </th>
                  <th> Количество </th>
                </tr>
              </thead>
              <tbody>
                #{
                  order.order_items.map do |order_item|
                    %{
                      <tr>
                        <td> #{ order_item.product.title } </td>
                        <td> #{ order_item.product.category.name } </td>
                        <td> #{ order_item.count } </td>
                      </tr>
                    }
                  end.join("\n")
                }
              </tbody>
            </table>
          }.html_safe
        end
      end

        field :delivery do
          pretty_value do
            order = bindings[:object]
            delivery_type = I18n.t("order.delivery_type.#{order.delivery_type}")
            %{
              <table class="table">
                <tbody>
                  <tr>
                    <th>Тип доставки</th>
                    <td>#{delivery_type}</td>
                  </tr>
                  <tr>
                    <th>Идентификатор в службе доставки</th>
                    <td>#{order.external_id}</td>
                  </tr>
                  <tr>
                    <th>ФИО адресата</th>
                    <td>#{order.full_name}</td>
                  </tr>
                  <tr>
                    <th>Страна</th>
                    <td>#{order.country}</td>
                  </tr>
                  <tr>
                    <th>Адрес</th>
                    <td>#{order.address}</td>
                  </tr>
                  <tr>
                    <th>Индекс</th>
                    <td>#{order.index}</td>
                  </tr>
                  <tr>
                    <th>Комментарий</th>
                    <td>#{order.comment}</td>
                  </tr>
                </tbody>
              </table>
            }.html_safe
          end
        end

    end
    navigation_label DATA_LABEL
    weight 1
  end

  # config.model "Delivery" do
  #   list do
  #     field :order
  #     field :comment
  #     field :cost
  #     field :country
  #     field :address
  #   end
  #
  #   navigation_label DATA_LABEL
  #   weight 2
  # end

  config.model "Review" do
    list do
      filters [:status]
      field :product
      field :user do
        pretty_value do
          value.try(:full_name)
        end
      end
      field :status, :enum do
        filterable true
        pretty_value do
          I18n.t("review.#{name}.#{value}")
        end
      end
    end

    show do
      field :user do
        pretty_value do
          value.try(:full_name)
        end
      end

      field :text do
        pretty_value do
          value.html_safe
        end
      end

      field :status, :enum do
        pretty_value do
          I18n.t("review.#{name}.#{value}")
        end
      end

      field :product
    end

    edit do
      field :user
      field :product
      field :status, :enum do
        pretty_value do
          I18n.t("review.#{name}.#{value}")
        end
      end
      field :text, :ck_editor
    end

    navigation_label DATA_LABEL
    weight 3
  end

  config.model "Page" do
    list do
      field :title
      field :url
    end

    show do
      include_all_fields

      field :content do
        pretty_value do
          value.html_safe
        end
      end
    end

    edit do
      field :title
      field :url
      field :content, :ck_editor
    end

    navigation_label DATA_LABEL
    weight 4
  end

  config.model "PackingCost" do
    list do
      field :cost do
        pretty_value do
          "#{value} RUB"
        end
      end
    end

    edit do
      field :cost
    end

    navigation_label DATA_LABEL
    weight 5
  end

  config.model "Preorder" do
    list do
      field :name
      field :start
      field :finish
    end

    edit do
      field :name
      field :description, :ck_editor
      field :start
      field :finish
      field :products
    end

    export do
      field :id
      field :name
      field :start
      field :finish
      field :products
    end

    navigation_label CATALOG_LABEL
    weight 0
  end

  config.model "Product" do
    list do
      field :title
      field :price
      field :in_stock
      field :category
      field :series
    end

    edit do
      field :title
      field :description, :ck_editor
      field :price
      field :in_stock
      field :category
      field :series

      field :countries
      field :rating
      field :status, :enum do
        pretty_value do
          I18n.t("product.#{name}.#{value}")
        end
      end
      field :russian_volumes
      field :original_volumes
      field :pages
      field :weight
      field :size
      field :article
      field :isbn
      field :format
      field :genres
      field :authors
      field :cover do
        pretty_value do
          product = bindings[:object]
          url = product.cover.url
          %{<img src='#{url}' class="img-responsive" style="max-width: 100px"/>}.html_safe
        end
      end
      field :preview_pdf
      field :preview_cbr
    end

    grid do
      thumbnail_method do
        :cover
      end
    end

    show do
      field :cover do
        label "Информация о товаре"
        pretty_value do
          product = bindings[:object]
          url = product.cover.url
          %{<style>
            .table td {
            white-space: normal !important;
            text-overflow: clip !important;
            }
            </style>
            <div class="container-fluid">
          <div class="row">
          <div class="col-sm-8">
            <table class="table">
              <tbody>
                <tr>
                  <th>Название</th>
                  <td>#{product.title}</td>
                </tr>
                <tr>
                  <th>Стоимость</th>
                  <td>#{product.price}</td>
                </tr>
                <tr>
                  <th>На складе</th>
                  <td>#{product.in_stock}</td>
                </tr>
                <tr>
                  <th>Категория</th>
                  <td>#{product.category.name}</td>
                </tr>
                <tr>
                  <th>Серия</th>
                  <td>#{product.series.name}</td>
                </tr>
                <tr>
                  <th>Страны</th>
                  <td>#{product.countries.map(&:name).join(", ")}</td>
                </tr>
                <tr>
                  <th>Жанры</th>
                  <td>#{product.genres.map(&:name).join(", ")}</td>
                </tr>
                <tr>
                  <th>Авторы</th>
                  <td>#{product.authors.map(&:full_name).join(", ")}</td>
                </tr>
                <tr>
                  <th>Рейтинг</th>
                  <td>#{product.rating}</td>
                </tr>
                <tr>
                  <th>Статус</th>
                  #{"<td>#{"#{I18n.t("product.status.#{product.status}")}, #{product.russian_volumes}/#{product.original_volumes}+"}</td>" if product.ongoing?}
                  #{"<td>#{"#{I18n.t("product.status.#{product.status}")}, #{product.russian_volumes}/#{product.original_volumes}"}</td>" if product.completed?}
                </tr>
                <tr>
                  <th>Количество страниц</th>
                  <td>#{product.pages}</td>
                </tr>
                <tr>
                  <th>Вес</th>
                  <td>#{product.weight}</td>
                </tr>
                <tr>
                  <th>Размер</th>
                  <td>#{product.size}</td>
                </tr>
                <tr>
                  <th>Артикул</th>
                  <td>#{product.article}</td>
                </tr>
                <tr>
                  <th>ISBN</th>
                  <td>#{product.isbn}</td>
                </tr>
                <tr>
                  <th>Формат</th>
                  <td>#{product.format}</td>
                </tr>
                <tr>
                  <th>Описание</th>
                  <td>#{product.description}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="col-sm-4">
          <img src='#{url}' class="img-responsive"/>
          </div>
          </div>
          </div>}.html_safe
        end
      end
    end

    navigation_label CATALOG_LABEL
    weight 1
  end

  config.model "Category" do
    list do
      field :name
    end

    edit do
      field :name
      field :description, :ck_editor
      field :products
    end

    navigation_label CATALOG_LABEL
    weight 2
  end

  config.model "Series" do
    list do
      field :name
    end

    edit do
      field :name
      field :description, :ck_editor
      field :products
    end

    navigation_label CATALOG_LABEL
    weight 3
  end

  config.model "Genre" do
    list do
      field :name
    end

    edit do
      field :name
      field :products
    end

    show do
      field :name
      field :products
    end

    navigation_label CATALOG_LABEL
    weight 4
  end

  config.model "Author" do
    list do
      field :full_name
    end

    edit do
      field :full_name
      field :description, :ck_editor
      field :products
    end

    show do
      field :full_name
      field :description
      field :products
    end

    navigation_label CATALOG_LABEL
    weight 5
  end

  config.model "Country" do
    list do
      field :name
    end

    edit do
      field :name
      field :description
      field :products
    end

    navigation_label CATALOG_LABEL
    weight 5
  end

  config.model "Slide" do
    list do
      field :title
    end

    edit do
      field :title
      field :img
      field :link
    end

    grid do
      thumbnail_method do
        :img
      end
    end

    navigation_label HOME_LABEL
    weight 0
  end

  config.model "Partner" do
    list do
      field :name
    end

    edit do
      field :name
      field :logo
      field :link
    end

    grid do
      thumbnail_method do
        :logo
      end
    end

    navigation_label HOME_LABEL
    weight 1
  end

  config.model "SocialNetwork" do
    field :name
    field :icon
    field :link

    navigation_label HOME_LABEL
    weight 2
  end

  config.model "Feature" do
    list do
      field :title
    end

    edit do
      field :title
      field :description
      field :icon
      field :page
    end

    navigation_label HOME_LABEL
    weight 0
  end

  config.model "AuthorProductMap" do
    visible false
  end

  config.model "GenreProductMap" do
    visible false
  end

  config.model "ProductImage" do
    visible false
  end

  config.model "Ckeditor::Asset" do
    visible false
  end

  config.model "Ckeditor::AttachmentFile" do
    visible false
  end

  config.model "Ckeditor::Picture" do
    visible false
  end

  config.model "PostcalcLightCity" do
    visible false
  end

  config.model "Admin" do
    visible false
  end

  config.model "OrderItem" do
    visible false
  end

  # config.model "Delivery" do
  #   visible false
  # end
end
