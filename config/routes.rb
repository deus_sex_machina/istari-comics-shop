Rails.application.routes.draw do
  devise_for :admins, controllers: {sessions: "admin/sessions"}
  mount Ckeditor::Engine => "/ckeditor"
  mount RailsAdmin::Engine => "/admin", as: "rails_admin"

  namespace :api, defaults: { format: :json } do
    namespace :protected do
      resources :products, only: [] do
        resources :reviews, only: :create
      end

      resources :postcalc, only: :index

      resources :users, only: :update do
        collection do
          get "current"
        end

        resources :orders, only: %i( index create ) do
          put "/merge/:old_order_id", action: :merge

          namespace :delivery do
            resources :post, only: :create
          end

          namespace :payment do
            resources :paymaster, only: :create
          end
        end
      end
    end

    namespace :callbacks do
      resources :orders, only: [] do
        namespace :delivery do
          resources :cdek, only: [] do
            collection do
              post "success"
              post "fail"
            end
          end

          resources :checkout, only: [] do
            collection do
              post "session"
              post "success"
            end
          end
        end
      end

      namespace :payment do
        resources :paymaster, only: [] do
          collection do
            post "notification"
            post "confirmation"
          end
        end
      end
    end

    mount_devise_token_auth_for "User",
      at: "auth",
      skip: %i( omniauth_callbacks ),
      controllers: { registrations: :registrations }

    resources :indices, only: :index
    resources :categories, only: :index
    resources :features, only: :index
    resources :pages, only: :index
    resources :preorders, only: :index
    resources :products, only: :index
    resources :series, only: :index
    resources :social_networks, only: :index
  end


  scope via: :all do
    root "front#index"

    %w{
      /sign-in
      /sign-up
      /page/:url
      /products/:id
      /categories/:id
      /series
      /series/:id
      /my/cart
      /my/orders
      /my/orders/:id/processing
      /my/orders/:id/payment
      /my/profile
    }.each { |path| match path, to: "front#index" }
  end
end
