var copFormId;
var cophost = 'platform.checkout.ru';

function copAddEvent(element, eventName, handler){
	if (element.addEventListener) {
		element.addEventListener(eventName, handler, false);
	} else {
		element.attachEvent("on" + eventName, handler);
	}
}

function closeCOP(){
	copIframe.location.href='about:blank';
	document.getElementById('copblock').style.display='none';
	for(var i = 0; i < document.body.childNodes.length; i++){
		if(document.body.childNodes[i].style){
			document.body.childNodes[i].className = document.body.childNodes[i].className.substring('hidemeonmobile '.length);
		}
	}
}

function openCheckout() {
  var copFormId = "checkout"
  if(copFormId == null || typeof(copFormId) != 'string') return;
  var formsIds = copFormId.split(',');
  for(var i = 0; i < formsIds.length; i++){
    var form = document.getElementById(formsIds[i]);
    if(form == null) continue;
    form.action = '//'+cophost+'/shop/checkout2';
    form.target = 'copIframe';
    form.method = 'post';
    copAddEvent(form, 'submit', function(event){
      for(var i = 0; i < document.body.childNodes.length; i++){
        if(document.body.childNodes[i].style){
          document.body.childNodes[i].className = 'hidemeonmobile ' + document.body.childNodes[i].className;
        }
      }
      document.getElementById('copblock').style.display='block';
    });
  }
  var div = document.createElement('DIV');
  div.id = 'copblock';
  div.innerHTML = '<link rel="stylesheet" type="text/css" href="//'+cophost+'/cop/cop.css?'+Math.random()+'" /><div class="copshadow"></div><div class="copcontent"><span id="closecop" onclick="closeCOP()"></span><iframe name="copIframe" id="copIframe" frameborder="0" height="100%" width="100%"></iframe></div>';
  document.body.appendChild(div);
}
